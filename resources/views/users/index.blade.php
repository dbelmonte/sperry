@extends('layouts.app')

@section('content')
<section class="col-md-8 offset-md-2 card mb-2">
    <div class="card-body">
        <a href="{{ route('user.create') }}" class="btn btn-success btn-small">Nuevo</a>
    </div>
</section>
<section class="col-md-8 offset-md-2 card">
    @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-hidden="true" aria-label="Close"></button>
    </div>
@elseif(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-hidden="true" aria-label="Close"></button>
    </div>
@endif
    <header class="card-header">
        <div class="card-actions">
            {{-- <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a> --}}
          
        </div>

        <h2 class="card-title">Usuarios</h2>
    </header>
    <div class="card-body">
        <table class="table table-responsive-md mb-0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Uusario</th>
                    <th>correo</th>
                    <th>Rol</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $i=>$user)
                <tr>
                    <td>{{ $i+1 }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>@foreach ($user->roles as $role) {{$role->name}} @endforeach</td>
                    <td class="actions">
                        <a href="{{ route('user.edit', $user->id) }}"><i class="fas fa-pencil-alt"></i></a>
                        <a href="#" onclick="deleteUser({{$user->id}})" class="delete-row"><i class="far fa-trash-alt"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>

@endsection
@push('scripts')
<script type="text/javascript">
    function deleteUser(id)
    {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        Swal.fire({
            title: "Eliminar Usuario",
            text: "¿Seguro de querer eliminar a este usuario?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            showLoaderOnConfirm: true,
            preConfirm: (option) => {
                let data = {
                    id: id,
                    csrf_token: csrf_token,
                    _method: 'DELETE'
                };
                return fetch('{{ route('user.destroy') }}', {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    method: 'POST',
                    body: JSON.stringify(data)
                })
                .then(response => {
                    if (!response.ok) {
                        throw new Error(response.statusText)

                    }
                    return response.json()
                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `Request failed: ${error}`
                    )
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.isConfirmed) {
                    console.log(result)
                    Swal.fire({
                        icon: `${result.value.icon}`,
                        title: `${result.value.msg}`,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                    }).then((result) => {
                        // Reload the Page
                        location.reload();
                    });
                }
        });
    }
  </script>
@endpush