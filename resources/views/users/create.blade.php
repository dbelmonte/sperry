@extends('layouts.app')

@section('content')
<section class="col-md-4 offset-md-4 card">
    @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-hidden="true" aria-label="Close"></button>
    </div>
@elseif(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-hidden="true" aria-label="Close"></button>
    </div>
@endif
    <header class="card-header">
        <h2 class="card-title">Usuarios</h2>
    </header>
    <div class="card-body">
        <form action="{{ route('user.store') }}" method="post" class="form-horizontal">
            @csrf
            <div class="form-group row pb-3">
                <label class="col-sm-4 control-label text-sm-end pt-2">Usuario:</label>
                <div class="col-sm-8">
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"  value="{{ old('name') }}">
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row pb-3">
                <label class="col-sm-4 control-label text-sm-end pt-2">Correo:</label>
                <div class="col-sm-8">
                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row pb-3">
                <label class="col-lg-4 control-label text-lg-end pt-2">Rol:</label>
                <div class="col-lg-8">
                    <select name="role" class="form-control populate">
                            <option value="Moderador">Moderador</option>
                    </select>
                </div>
            </div>
            <div class="form-group row pb-3">
                <label class="col-sm-4 control-label text-sm-end pt-2">Contraseña:</label>
                <div class="col-sm-8">
                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <footer class="card-footer text-end">
                <button class="btn btn-primary">Crear</button>
            </footer>
        </form>
    </div>
</section>

@endsection