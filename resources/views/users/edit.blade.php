@extends('layouts.app')

@section('content')
<section class="col-md-4 offset-md-4 card">
    <header class="card-header">
        <h2 class="card-title">Usuarios</h2>
    </header>
    <div class="card-body">
      <form action="{{ route('user.update', $user->id) }}" method="post" class="form-horizontal">
        @csrf
        @method('PUT')
        <div class="form-group row pb-3">
            <label class="col-sm-4 control-label text-sm-end pt-2">Usuario:</label>
            <div class="col-sm-8">
                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"  value="{{ $user->name ? $user->name : old('name') }}">
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row pb-3">
            <label class="col-sm-4 control-label text-sm-end pt-2">Correo:</label>
            <div class="col-sm-8">
                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ $user->email ? $user->email : old('email') }}">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row pb-3">
            <label class="col-lg-4 control-label text-lg-end pt-2">Rol:</label>
            <div class="col-lg-8">
                <select name="role" class="form-control populate">
                        <option value="Moderador">Moderador</option>
                </select>
            </div>
        </div>
        <div class="form-group row pb-3">
            <label class="col-sm-4 control-label text-sm-end pt-2">Contraseña:</label>
            <div class="col-sm-8">
                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <footer class="card-footer text-end">
            <button class="btn btn-primary">Actualizar</button>
        </footer>
    </form>
    </div>
</section>

@endsection