@extends('layouts.app')

@section('content')
<div class="col-lg-6 offset-md-3">
    <section class="card">
        <header class="card-header">

            <h2 class="card-title">Editar Dirección de Facturación</h2>
        </header>
        <form action="{{ route('address.update', $address->id) }}" method="POST">
        <div class="card-body">
                @csrf
                @method('PUT')
                <div class="row form-group pb-3">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="col-form-label" for="nombre">Nombre</label>
                            <input type="text" name="nombre" class="form-control @error('nombre') is-invalid @enderror" id="nombre" value="{{ $address->nombre ? $address->nombre : old('nombre') }}">
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="col-form-label" for="apellido_1">Apellido 1</label>
                            <input type="text" name="apellido_1" class="form-control @error('apellido_1') is-invalid @enderror" id="apellido_1"  value="{{ $address->apellido_1 ? $address->apellido_1 : old('apellido_1') }}">
                            @error('apellido_1')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="col-form-label" for="apellido_2">Apellido 2</label>
                            <input type="text" name="apellido_2" class="form-control @error('apellido_2') is-invalid @enderror" id="apellido_2"  value="{{ $address->apellido_2 ? $address->apellido_2 : old('apellido_2') }}">
                            @error('apellido_2')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row form-group pb-3">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="col-form-label" for="calle">Calle</label>
                            <input type="text" name="calle" class="form-control @error('calle') is-invalid @enderror" id="calle" value="{{ $address->calle ? $address->calle : old('calle') }}">
                            @error('calle')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label class="col-form-label" for="no_interior">Num. Interior</label>
                            <input type="text" name="no_interior" class="form-control @error('no_interior') is-invalid @enderror" id="no_interior"  value="{{ $address->no_interior ? $address->no_interior : old('no_interior') }}">
                            @error('no_interior')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label class="col-form-label" for="no_exterior">Num. Exterior</label>
                            <input type="text" name="no_exterior" class="form-control @error('no_exterior') is-invalid @enderror" id="no_exterior"  value="{{ $address->no_exterior ? $address->no_exterior : old('no_exterior') }}">
                            @error('no_exterior')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="col-form-label" for="colonia">Colonia</label>
                            <input type="text" name="colonia" class="form-control @error('colonia') is-invalid @enderror" id="colonia"  value="{{ $address->colonia ? $address->colonia : old('colonia') }}">
                            @error('colonia')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row form-group pb-3">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="col-form-label" for="poblacion">Población</label>
                            <input type="text" name="poblacion" class="form-control @error('poblacion') is-invalid @enderror" id="poblacion" value="{{ $address->poblacion ? $address->poblacion : old('poblacion') }}">
                            @error('poblacion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="col-form-label" for="provincia">Provincia</label>
                            <input type="text" name="provincia" class="form-control @error('provincia') is-invalid @enderror" id="provincia" value="{{ $address->provincia ? $address->provincia : old('provincia') }}">
                            @error('provincia')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label class="col-form-label" for="codigo_postal">Codigo Postal</label>
                            <input type="text" name="codigo_postal" class="form-control @error('codigo_postal') is-invalid @enderror" id="codigo_postal" value="{{ $address->codigo_postal ? $address->codigo_postal : old('codigo_postal') }}">
                            @error('codigo_postal')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="col-form-label" for="telefono">Teléfono</label>
                            <input type="text" name="telefono" class="form-control @error('telefono') is-invalid @enderror" id="telefono" value="{{ $address->telefono ? $address->telefono : old('telefono') }}">
                            @error('telefono')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    
                </div>
            </div>
            <footer class="card-footer text-end">
                <a href="{{ route('order.show', $address->id_orden) }}" class="btn btn-secondary btn-small">Volver</a>
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </footer>
        </form>
    </section>
</div>
@endsection