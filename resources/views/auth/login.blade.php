@extends('layouts.page')

@section('content')
<div class="row justify-content-center align-items-center authentication authentication-basic h-100">
    <div class="col-xxl-4 col-xl-5 col-lg-5 col-md-6 col-sm-8 col-12">
        <div class="my-5 d-flex justify-content-center">
            <a href="index.html">
                <img src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}" class="desktop-logo">
                <img src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}" class="desktop-dark">
            </a>
        </div>
        <div class="card custom-card">
            <div class="card-body p-5">
                <p class="h5 fw-semibold mb-2 text-center">LOGIN</p>
                <div class="row gy-3">
                    <form action="{{ route('login') }}" method="post">
                        @csrf
                        <div class="col-xl-12">
                            <label for="signin-username" class="form-label text-default">Usuario</label>
                            <input name="email" value="{{ old('email') }}" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" id="signin-username" placeholder="Usuario" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-xl-12 mb-2">
                            <label for="signin-password" class="form-label text-default d-block">Contraseña</label>
                            <div class="input-group">
                                <input name="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" id="signin-password" placeholder="Contraseña" required autocomplete="current-password">
                                <button class="btn btn-light" type="button" onclick="createpassword('signin-password',this)" id="button-addon2"><i class="ri-eye-off-line align-middle"></i></button>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-2">
                                <div class="form-check">
                                    <input class="form-check-input" id="remember" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label text-muted fw-normal" for="defaultCheck1">
                                        Recordarme
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 d-grid mt-2">
                            <button type="submit"  class="btn btn-lg btn-primary">Aceptar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
