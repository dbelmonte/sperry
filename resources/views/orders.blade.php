@extends('layouts.app')
@section('content')
   <!-- Page Header -->
<div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
    <h1 class="page-title fw-semibold fs-18 mb-0">ORDENES</h1>
    <div class="ms-md-1 ms-0">
        <nav>
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                <li class="breadcrumb-item active" aria-current="page">Ordenes</li>
            </ol>
        </nav>
    </div>
</div>
<!-- Page Header Close -->

  <!-- Start::row-1 -->
  <div class="row">
    <div class="col-xl-12">
        <div class="card custom-card">
            <div class="card-header">
                <div class="card-title">
                    Ordenes
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered" id="orders-table">
                    <thead>
                        <tr>
                            <th>ORDEN</th>
                            <th>NO HERMES</th>
                            <th>FECHA</th>
                            <th>CLIENTE</th>
                            <th>TOTAL</th>
                            <th>ESTATUS</th>
                            <th>ENVIO</th>
                            <th>COSTO ENVIO</th>
                            <th>METODO PAGO</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!--End::row-1 -->


{{-- <section class="card mb-4">
    <div class="card-body">
        <button class="btn btn-info" id="procesar-orden">Procesar Orden</button>
    </div>
</section> --}}

@stop
@include('assets.datatables')
@push('scripts')
    <script>
        $(function() {
            $('#orders-table').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                },
                "pageLength": 10,
                processing: true,
                serverSide: true,
                ajax: '{!! route('order.data') !!}',
                columns: [{
                        data: 'orden_num',
                        name: 'orden_num'
                    },
                    {
                        data: 'id_orden_hermes',
                        name: 'id_orden_hermes'
                    },
                    {
                        data: 'fecha_pedido',
                        name: 'fecha_pedido'
                    },
                    {
                        data: 'full_name',
                        name: 'full_name'
                    },
                    {
                        data: 'importe_total',
                        name: 'importe_total'
                    },
                    {
                        data: 'payment_status',
                        name: 'payment_status'
                    },
                    {
                        data: 'shipping_method',
                        name: 'shipping_method'
                    },
                    {
                        data: 'shipping_price',
                        name: 'shipping_price'
                    },
                    {
                        data: 'payment_method',
                        name: 'payment_method'
                    },
                    {
                        data: 'options',
                        name: '',
                        orderable: false,
                        searchable: false
                    },

                ],
                order: [
                    [2, 'desc']
                ]
            });

         

        });
    </script>
@endpush
