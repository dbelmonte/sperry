@extends('layouts.app')

@section('content')
    <div class="row mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Busqueda Avanzada</h5>
                </div>
                <div class="card-body">
                    <div class="float-right" id="advanced-search-wrapper">
                        <button type="button" class="btn btn-primary" id="bttnShowAll" style="display:none;">Mostrar
                            todos</button>
                        <button type="button" class="btn btn-primary" id="bttn-advanced-search">
                            <i class="fas fa-search"></i> Búsqueda Avanzada
                        </button>
                        <span id="msjExport"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">

                <table class="table table-bordered table-sm" id="newsletter-table">
                    <thead>
                        <tr>
                            <th>CREADO</th>
                            <th>NOMBRE</th>
                            <th>APELLIDO</th>
                            <th>CORREO</th>
                            <th>TELEFONO</th>
                            <th>FECHA NAC.</th>
                            <th>GENERO</th>
                            <th>ESTADO</th>
                            <th>NL_TODAS LAS ANTERIORES</th>
                            <th>NL_MUJER</th>
                            <th>NL_HOMBRE</th>
                            <th>NL_NIÑOS</th>
                            <th>NL_MOCACIONES</th>
                            <th>NL_TENIS</th>
                            <th>NL_SANDALIAS</th>
                            <th>NL_FLATS</th>
                            <th>NL_BOTAS</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@stop
@push('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript"
        src="https://cdn.datatables.net/v/bs5/jszip-2.5.0/dt-1.11.3/b-2.1.1/b-colvis-2.1.1/b-html5-2.1.1/b-print-2.1.1/datatables.min.js">
    </script>

    <script>
        $(function() {
            function getTableData(tablename, params) {
                $('#' + tablename).DataTable().clear();
                $('#' + tablename).DataTable().destroy();
                $('#' + tablename).DataTable({
                    dom: 'Bfrtip',
                    buttons: [{
                            extend: 'csvHtml5',
                            title: 'registros-nesletter'
                        },
                        {

                            extend: 'excelHtml5',
                            title: 'registros-nesletter'
                        }
                    ],
                    ajax: {
                        "method": "GET",
                        "data": {
                            "params": params
                        },
                        "url": "{!! route('newsletter.data') !!}"
                    },
                    columns: [{
                            data: 'created_at',
                            name: 'created_at'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'lastname',
                            name: 'lastname'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'phone',
                            name: 'phone'
                        },
                        {
                            data: 'birthdate',
                            name: 'birthdate'
                        },
                        {
                            data: 'gender',
                            name: 'gender'
                        },
                        {
                            data: 'state',
                            name: 'state'
                        },
                        {
                            data: 'categories',
                            name: 'categories'
                        },
                        {
                            data: 'category_1',
                            name: 'categoria 1'
                        },
                        {
                            data: 'category_2',
                            name: 'categoria 2'
                        },
                        {
                            data: 'category_3',
                            name: 'categoria 3'
                        },
                        {
                            data: 'product_1',
                            name: 'product_1'
                        },
                        {
                            data: 'product_2',
                            name: 'product_2'
                        },
                        {
                            data: 'product_3',
                            name: 'product_3'
                        },
                        {
                            data: 'product_4',
                            name: 'product_4'
                        },
                        {
                            data: 'product_5',
                            name: 'product_5'
                        }
                    ],
                    order: [
                        [0, 'asc']
                    ]
                });

            }
            getTableData('newsletter-table', 'all');

            $('#bttn-advanced-search').click(function() {

                bootbox.confirm({
                    title: "Buscar por fecha",
                    message: '<form id="some-form">' +
                        '<div class="form-group">' +
                        '<label for="date">Date</label>' +
                        '<input id="date_before" class="date span2 form-control input-sm" size="16" placeholder="mm/dd/yy" type="date" required>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<label for="date">Date</label>' +
                        '<input id="date_after" class="date span2 form-control input-sm" size="16" placeholder="mm/dd/yy" type="date" required>' +
                        '</div>' +
                        '</form>',
                    callback: function(result) {
                        var date_before = $('#date_before').val();
                        var date_after = $('#date_after').val();
                        var params = JSON.stringify({
                            'date_before': date_before,
                            'date_after': date_after
                        });
                        console.log(params);
                        console.log(result)
                        if (date_after != '' && date_before != '') {
                            getTableData('newsletter-table', params);
                            bootbox.hideAll();

                            $('#bttnShowAll').show();

                            $('#bttnShowAll').click(function() {
                                $(this).hide();
                                getTableData('newsletter-table', 'all');
                            });
                        }
                    }
                });
            });
        });
    </script>
@endpush
