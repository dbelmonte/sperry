<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>MODELO</th>
        <th>DESCRIPCION</th>
        <th>URL PROD.</th>
        <th>ESTADO</th>
        <th>URL IMG</th>
        <th>PRECIO</th>
        <th>PRECIO OFERT.</th>
        <th>FECHA PROM.</th>
        <th>MARCA</th>
        <th>MATERIAL</th>
        <th>DISPONIBILIDAD</th>
        <th>STOCK</th>
        <th>GENERO</th>
        <th>EDAD</th>
        <th>CATEGORIA</th>
        <th>CATEGORIA ID</th>
        <th>APLAZO</th>
        <th>TALLA</th>
        <th>SIST. TALLA</th>
        <th>UPC BARCODE</th>
        <th>COLOR</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr>
            <td>{{ $product->id }}</td>
            <td>{{ $product->nombre }}</td>
            <td>{{ $product->descripcion }}</td>
            <td>{{ $product->url_product }}</td>
            <td>{{ $product->estado }}</td>
            <td>{{ $product->url_imagen }}</td>
            <td>{{ $product->precio }}</td>
            <td>{{ $product->precio_oferta }}</td>
            <td>{{ $product->fecha_promo }}</td>
            <td>{{ $product->marca }}</td>
            <td>{{ $product->material }}</td>
            <td>{{ $product->disponibilidad }}</td>
            <td>{{ $product->stock }}</td>
            <td>{{ $product->genero }}</td>
            <td>{{ $product->edad }}</td>
            <td>{{ $product->categoria }}</td>
            <td>{{ $product->categoria_2 }}</td>
            <td>{{ $product->installment_amount }}</td>
            <td>{{ $product->talla }}</td>
            <td>{{ $product->sistema_talla }}</td>
            <td>{{ $product->upc_barcode }}</td>
            <td>{{ $product->color }}</td>
        </tr>
    @endforeach
    </tbody>
</table>