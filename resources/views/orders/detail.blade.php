@extends('layouts.app')

@section('content')
@include('layouts.messages.messages')

 <!-- Page Header -->
 <div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
    <h1 class="page-title fw-semibold fs-18 mb-0">Orden Detalle</h1>
    <div class="ms-md-1 ms-0">
        <nav>
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="#">Ordenes</a></li>
                <li class="breadcrumb-item active" aria-current="page">Orden Detalle</li>
            </ol>
        </nav>
    </div>
</div>
<!-- Page Header Close -->

<!-- Start::row-1 -->
<div class="row">
    <div class="col-xl-3">
        <div class="row">
            <div class="col-xl-12">
                <div class="card custom-card">
                    <div class="card-header">
                        <div class="card-title">
                           Detalles de Cliente
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="d-flex align-items-center border-bottom border-block-end-dashed p-3 flex-wrap">
                            <div class="me-2">
                                <span class="avatar avatar-lg avatar-rounded">
                                    <img src="{{ asset('images/user.png') }}" alt="">
                                </span>
                            </div>
                            <div class="flex-fill">
                                <p class="mb-0">{{ $order->getFullNameCustomerAttribute() }}</p>
                                <p class="mb-0 text-muted fs-12">{{ $order->email_envio }}</p>
                            </div>
                        </div>
                        <div class="p-3 border-bottom border-block-end-dashed">
                            <div class="d-flex align-items-center justify-content-between mb-3">
                                <span class="fs-14 fw-semibold">Datos cliente:</span>
                                <button class="btn btn-icon btn-wave btn-primary btn-sm" id="add-rfc" data-id="{{ $order->id_orden }}"><i class="ri-pencil-line"></i></button>
                            </div>
                            <p class="mb-2 text-muted"><span class="fw-semibold text-default">RFC : </span>{{ $order->cliente_rfc ? $order->cliente_rfc : 'N/A'}}</p>
                            <p class="mb-2 text-muted"><span class="fw-semibold text-default">CFDI : </span>{{ $order->cfdi_code ? $order->cfdi_code : 'N/A'}}</p>
                            <p class="mb-2 text-muted"><span class="fw-semibold text-default">USO CFDI : </span>{{ $order->uso_cfdi ? $order->uso_cfdi : 'N/A' }}</p>
                        </div>
                        @foreach ($addressess as $address)
                            @if ($address->tipo == 'billing')
                            <div class="p-3 border-bottom border-block-end-dashed">
                                <div class="d-flex align-items-center justify-content-between mb-3">
                                    <span class="fs-14 fw-semibold">Dirección de Facturación :</span>
                                    <a href="{{ route('address.edit', $address->id_orden) }}" class="btn btn-icon btn-wave btn-primary btn-sm"><i class="ri-pencil-line"></i></a>
                                </div>
                                <p class="mb-2 text-muted"><span class="fw-semibold text-default">Nombre Y Apellido : </span>{{ $address->nombre }} {{ $address->apellido_1 }} {{ $address->apellido_2 }}</p>
                                <p class="mb-2 text-muted"><span class="fw-semibold text-default">Colonia : </span>{{ $address->colonia }} {{ $address->calle }}</p>
                                <p class="mb-2 text-muted"><span class="fw-semibold text-default">No. Exterior / Exterior : </span>{{ $address->no_interior }} {{ $address->no_exterior }}</p>
                                <p class="mb-2 text-muted"><span class="fw-semibold text-default">Provincia : </span>{{ $address->poblacion }} {{ $address->provincia }}</p>
                                <p class="mb-2 text-muted"><span class="fw-semibold text-default">Codigo Postal : </span>{{ $address->codigo_postal }}</p>
                                <p class="mb-0 text-muted"><span class="fw-semibold text-default">Pais : </span>{{ $address->pais }}</p>
                                <p class="mb-0 text-muted"><span class="fw-semibold text-default">Teléfono : </span>{{ $address->telefono }}</p>
                            </div>
                            @endif
                        @endforeach
                        @foreach ($addressess as $address)
                            @if ($address->tipo == 'shipping')
                            <div class="p-3 border-bottom border-block-end-dashed">
                                <div class="d-flex align-items-center justify-content-between mb-3">
                                    <span class="fs-14 fw-semibold">Dirección de Envío :</span>
                                </div>
                                <p class="mb-2 text-muted"><span class="fw-semibold text-default">Nombre Y Apellido : </span>{{ $address->nombre }} {{ $address->apellido_1 }} {{ $address->apellido_2 }}</p>
                                <p class="mb-2 text-muted"><span class="fw-semibold text-default">Colonia : </span>{{ $address->colonia }} {{ $address->calle }}</p>
                                <p class="mb-2 text-muted"><span class="fw-semibold text-default">No. Exterior / Exterior : </span>{{ $address->no_interior }} {{ $address->no_exterior }}</p>
                                <p class="mb-2 text-muted"><span class="fw-semibold text-default">Provincia : </span>{{ $address->poblacion }} {{ $address->provincia }}</p>
                                <p class="mb-2 text-muted"><span class="fw-semibold text-default">Codigo Postal : </span>{{ $address->codigo_postal }}</p>
                                <p class="mb-0 text-muted"><span class="fw-semibold text-default">Pais : </span>{{ $address->pais }}</p>
                                <p class="mb-0 text-muted"><span class="fw-semibold text-default">Teléfono : </span>{{ $address->telefono }}</p>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-9">
        <div class="row">
            <div class="col-xl-12">
                <div class="card custom-card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="card-title">
                            Orden No - <span class="text-primary">#{{ $order->orden_num }}</span>
                        </div>
                        <div>
                            <span class="badge bg-primary-transparent">
                                Fecha: {{ Carbon\Carbon::parse($order->fecha_pedido)->format('d-m-Y') }}
                            </span>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                    <tr>
                                        <th scope="col">FACTURA</th>
                                        <th scope="col">FOLIO</th>
                                        <th scope="col">CONFIRMACIÓN</th>
                                        <th scope="col">ESTATUS</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($edicom as $factura)
                                    <tr>
                                        <td class="table-active {!! tableClass($factura->serie) !!}">
                                            {{ $factura->serie }}{{ $factura->folio }}
                                        </td>
                                         <td>
                                            {{ $factura->folio }}
                                        </td>
                                        <td>
                                            @if ($factura->bill_status == 'confirmed' && $factura->verification_code)
                                            <span class="badge bg-success-transparent">{{ $factura->verification_code }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            {!! get_bill_status($factura->bill_status) !!}
                                            {{-- <span class="badge bg-success">Completed</span>
                                            <span class="badge bg-danger">Failed</span>
                                            <span class="badge bg-dark">Generated</span> --}}
                                        </td>
                                        <td>
                                            <div style="display: inline-flex">
                                                @if ($factura->bill_status == 'confirmed' && $factura->verification_code)
                                                <form action="{{ route('order.download') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="folio" value="{{ $factura->serie.$factura->folio }}">
                                                    <button type="submit" name="download-pdf" class="btn btn-icon btn-dark  btn-wave btn-sm">
                                                        <i class="ri-download-2-line"></i>
                                                    </button>
                                                </form>
                                                @endif
                                                @if ($factura->serie == config('app.SERIE_NC'))
                                                <a href="{{ route('order.show_nc', [$order->id_orden, $factura->folio]) }}" class="btn btn-icon btn-info  btn-wave btn-sm"><i class="ri-eye-fill"></i></a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer border-top-0">
                        <div class="btn-list float-end">
                            <!-- <button class="btn btn-primary btn-wave btn-sm" onclick="javascript:window.print();"><i class="ri-printer-line me-1 align-middle d-inline-block"></i>Print</button> -->
                            <button class="btn btn-success btn-wave btn-sm" id="gen_factura" data-id="{{ $order->id_orden }}"><i class="ri-share-forward-line me-1 align-middle d-inline-block"></i>Generar Factura</button>
                            <button class="btn btn-warning btn-wave btn-sm" id="hermes" data-id="{{ $order->id_orden }}"><i class="ri-share-forward-line me-1 align-middle d-inline-block"></i>Llamada HERMES</button>
                            <button class="btn btn-danger btn-wave btn-sm"  id="change_date" data-id="{{ $order->id_orden }}"><i class="ri-share-forward-line me-1 align-middle d-inline-block" data-bs-toggle="tooltip"
                                data-bs-placement="bottom" title="Fambiar la fecha del pedido" ></i>Cambiar Fecha</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="card custom-card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="card-title">
                            Productos de la orden
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="products" class="table table-no-more table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-start">Line Item ID</th>
                                        <th class="text-start">Talla</th>
                                        <th class="text-start">Producto</th>
                                        <th class="text-start">Codigo Alfa</th>
                                        <th class="text-start">Referencia</th>
                                        <th class="text-start">Cantidad</th>
                                        <th class="text-start">Total</th>
                                        <th class="text-start">Facturado</th>
                                        {{-- <th class="text-start"></th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $product)
                                        <tr>
                                            <td></td>
                                            <td data-title="line_item">{{ $product->line_item_id }}</td>
                                            <td data-title="line_item">{{ $product->codigo_alfa }}-{{ $product->talla }}</td>
                                            <td data-title="Nombre">{{ $product->title }}</td>
                                            <td data-title="Codigo ALFA">{{ $product->codigo_alfa }}</td>
                                            <td data-title="Referencia">{{ $product->referencia }}</td>
                                            <td data-title="Cantidad">{{ $product->cantidad }}
                                                @if ($product->nota_credito > 0)
                                                    <span class="badge bg-danger">Devueltos: {{ $product->nota_credito }} </span>
                                                @endif
                                            </td>
                                            <td data-title="Precio">{{ $product->importe }}</td>
                                            <td data-title="Facturado">@if ($product->facturado) Si @else No @endif</td>
                                            {{-- <td>
            
                                                <button class="mb-1 mt-1 me-1 btn btn-xs btn-danger nota_credito"
                                                    onclick="notaCredito({{ $product->id }}, {{ $product->cantidad }})"
                                                    title="Estatus NOTA CREDITO" data-id="{{ $product->id }}"
                                                    data-qty="{{ $product->cantidad }}"><i class="fa fa-not-equal"></i></button>
            
                                            </td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                      
                    </div>
                    <div class="card-footer border-top-0">
                        <div class="btn-list float-end">
                            <!-- <button class="btn btn-primary btn-wave btn-sm" onclick="javascript:window.print();"><i class="ri-printer-line me-1 align-middle d-inline-block"></i>Print</button> -->
                            <button class="btn btn-danger btn-wave btn-sm"  id="gen_nc" data-id="{{ $order->id_orden }}"><i class="ri-share-forward-line me-1 align-middle d-inline-block" ></i>Nota de Credito</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End::row-1 -->
@stop
@push('styles')
    <style>
        .swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
}
        </style>
@endpush
@include('assets.datatables')
@push('scripts')
    <script>
        // function notaCredito(product_id, qty) {
        //     console.table(product_id, qty)
        //     var token =
        //         '{{ csrf_token() }}';

        //     Swal.fire({
        //         title: 'Crear devolución',
        //         text: "Crear una devolución de este articulo, ingrese la cantidad que desea devolver.",
        //         icon: 'warning',
        //         showCancelButton: true,
        //         confirmButtonText: 'Aceptar',
        //         showLoaderOnConfirm: true,
        //         input: 'number',
        //         inputAttributes: {
        //             min: 1,
        //             max: qty
        //         },
        //         inputValidator: (value) => {
        //             return new Promise((resolve) => {
        //                 if (value > qty) {
        //                     resolve(
        //                         'La cantidad ingresada es mayor a la cantidad de unidades del articulo.'
        //                     )
        //                 } else if (value < 1) {
        //                     resolve('La cantidad ingresada debe ser 1 o mayor.')
        //                 } else {
        //                     resolve()
        //                 }
        //             })
        //         },
        //         preConfirm: (qty) => {
        //             let data = {
        //                 id: product_id,
        //                 _token: token,
        //                 cantidad: qty
        //             };
        //             return fetch('{{ route('product.delete') }}', {
        //                     headers: {
        //                         'Content-Type': 'application/json',
        //                         'X-CSRF-TOKEN': '{{ csrf_token() }}'
        //                     },
        //                     method: 'POST',
        //                     body: JSON.stringify(data),
        //                 })
        //                 .then(response => {
        //                     if (!response.ok) {
        //                         throw new Error(response.statusText)

        //                     }
        //                     return response.json()
        //                 })
        //                 .catch(error => {
        //                     Swal.showValidationMessage(
        //                         `Request failed: ${error}`
        //                     )
        //                 })
        //         },
        //         allowOutsideClick: () => !Swal.isLoading()
        //     }).then((result) => {
        //         if (result.isConfirmed) {
        //             console.log(result)
        //             Swal.fire({
        //                 icon: `${result.value.icon}`,
        //                 title: `${result.value.msg}`,
        //                 allowOutsideClick: false,
        //                 allowEscapeKey: false,
        //             }).then((result) => {
        //                 // Reload the Page
        //                 location.reload();
        //             });
        //         }
        //     })
        // }
        $(document).ready(function() {

            $("#change_date").click(function(e) {
                e.preventDefault();
                const orden = document.querySelector('#change_date');
                var token = '{{ csrf_token() }}';
                
                Swal.fire({
                    title: 'Cambiar fecha de Pedido',
                    html: '<input id="datepicker" readonly class="swal2-input">',
                    customClass: 'swal2-overflow',
                    showConfirmButton: true,
                    showCancelButton: true,
                    willOpen: () => {
                        $('#datepicker').datepicker({
                            dateFormat: 'dd-mm-yy'
                        });
                    },
                    preConfirm: () => {
                        return new Promise(function (resolve, reject) {
                            // Validate input
                            if ($('#datepicker').val() == '') {
                                Swal.disableButtons();	 // Enable the confirm button again.
                                swal.showValidationMessage("Ingrese valores validos"); // Show error when validation fails.
                            } else {
                                swal.resetValidationMessage(); // Reset the validation message.
                                resolve(updateDate(orden.dataset.id,$('#swal-input1').val()));
                            }
                        })
                    },
                }).then((result) => {
                    // console.log(JSON.stringify(result.value[0]));
                    // console.log(JSON.stringify(result.value[0]));
                    if (result.isConfirmed) {
                        console.log(result)
                        // Swal.fire({
                        //     icon: `${result.value.icon}`,
                        //     title: `${result.value.msg}`,
                        //     allowOutsideClick: false,
                        //     allowEscapeKey: false
                        // }).then((result) => {
                        //     // Reload the Page
                        //     location.reload();
                        // });
                    }
                });
                
            });

            function updateDate(order, dateOrder){
                var token =
                '{{ csrf_token() }}';
                    let data = {
                        id: order,
                        date: dateOrder,
                        _token: token
                    };
                return fetch('{{ route('order.updateDate') }}', {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    method: 'POST',
                    body: JSON.stringify(data)
                })
                .then(response => {
                    if (!response.ok) {
                        throw new Error(response.statusText)

                    }
                    return response.json()
                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `Request failed: ${error}`
                    )
                })
            };


            var table = $('#products').DataTable( {  
                "searching": false,   
                    'columnDefs': [
                        {
                            'targets': 0,
                            'checkboxes': {
                            'selectRow': true
                            }
                        }
                    ],
                    'select': {
                        'style': 'multi'
                    },
                    'order': [[1, 'asc']]
                } );

            $('#products tbody').on('click', 'tr', function () {
                console.log(this);
                    $(this).toggleClass('selected');
            });
            var FacturainputOptions = new Promise(function(resolve) {
                resolve({
                    'S01': 'S01 - Sin Efectos Fiscales',
                    'G01': 'G01 - Adquisición de mercancías',
                    'G03': 'G03 - Gastos en general',
                    'D04': 'D04 - Donativos',
                    'I01': 'I01 - Construcciones'
                });
            });
            $("#gen_factura").click(function(e) {
                e.preventDefault();
                const orden = document.querySelector('#gen_factura');
                var token =
                    '{{ csrf_token() }}';
                
                Swal.fire({
                    title: 'Generar Factura',
                    text: "Genera una factura que envias directamente a edicom",
                    icon: 'info',
                    input: 'select',
                    inputValue: 'S01',
                    inputOptions: FacturainputOptions,
                    showCancelButton: true,
                    confirmButtonText: 'Aceptar',
                    showLoaderOnConfirm: true,
                    inputValidator: function(option) {
                        return new Promise(function(resolve, reject) {
                            if (option != '' || option != null) {
                                resolve();
                            } else {
                                reject('Selecciona una opción');
                            }
                        });
                    },
                    preConfirm: (option) => {
                        let data = {
                            id: orden.dataset.id,
                            option: option,
                            _token: token
                        };
                        return fetch('{{ route('order.facturar') }}', {
                                headers: {
                                    'Content-Type': 'application/json',
                                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                                },
                                method: 'POST',
                                body: JSON.stringify(data)
                            })
                            .then(response => {
                                if (!response.ok) {
                                    throw new Error(response.statusText)

                                }
                                return response.json()
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        console.log(result)
                        Swal.fire({
                            icon: `${result.value.icon}`,
                            title: `${result.value.msg}`,
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then((result) => {
                            // Reload the Page
                            location.reload();
                        });
                    }
                })
            });
            $("#gen_nc").click(function(e) {
                e.preventDefault();
                var array = table.rows('.selected').data().toArray();
                var products = {'dataToDelete': array};
                const orden = document.querySelector('#gen_nc');
                
                Swal.fire({
                    title: 'Generar Nota de Credito',
                    text: 'Debe tener seleccionado uno o más productos',
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonText: 'Aceptar',
                    showLoaderOnConfirm: true,
                    focusConfirm: false,
                    preConfirm: () => {
                        if (array == '') {
                            Swal.showValidationMessage(
                                'Seleccione uno o más productos'
                            )
                        }
                        var token =
                            '{{ csrf_token() }}';0
                            let data = {
                                id: orden.dataset.id,
                                products: products,
                                _token: token
                            };
                        return fetch('{{ route('order.gen_nc') }}', {
                                headers: {
                                    'Content-Type': 'application/json',
                                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                                },
                                method: 'POST',
                                body: JSON.stringify(data)
                            })
                            .then(response => {
                                if (!response.ok) {
                                    throw new Error(response.statusText)

                                }
                                return response.json()
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            })
                            // console.log(resolve)
                    },
                            
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    // console.log(JSON.stringify(result.value[0]));
                    // console.log(JSON.stringify(result.value[0]));
                    if (result.isConfirmed) {
                        console.log(result.value.return_url)
                        Swal.fire({
                            icon: `${result.value.icon}`,
                            title: `${result.value.msg}`,
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then(() => {
                            // Reload the Page
                            window.location.href = result.value.return_url;
                        });
                    }
                })
            });

            function sendNC(order, products)
            {
                var token =
                '{{ csrf_token() }}';
                    let data = {
                        id: order,
                        products: products,
                        _token: token
                    };
                    return fetch('{{ route('order.notacredito') }}', {
                            headers: {
                                'Content-Type': 'application/json',
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            method: 'POST',
                            body: JSON.stringify(data)
                        })
                        .then(response => {
                            if (!response.ok) {
                                throw new Error(response.statusText)

                            }
                            return response.json()
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
            }
            var inputOptionsNC = new Promise(function(resolve) {
                resolve({
                    'CANCELACION': 'CANCELACION',
                    'RESERVA': 'RESERVA',
                    'VENTA': 'VENTA'
                });
            });
            $("#hermes").click(function(e) {
                e.preventDefault();
                const orden = document.querySelector('#hermes');
                var token =
                    '{{ csrf_token() }}';
                Swal.fire({
                    title: 'ENVIAR LLAMADA HERMES',
                    text: "Enviar la llamada de la orden a HERMES.",
                    icon: 'info',
                    input: 'radio',
                    inputValue: 'CANCELACION',
                    inputOptions: inputOptionsNC,
                    showCancelButton: true,
                    confirmButtonText: 'Aceptar',
                    showLoaderOnConfirm: true,
                    preConfirm: (option) => {
                        let data = {
                            id: orden.dataset.id,
                            opcion: option,
                            _token: token
                        };
                        return fetch('{{ route('order.hermes') }}', {
                                headers: {
                                    'Content-Type': 'application/json',
                                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                                },
                                method: 'POST',
                                body: JSON.stringify(data)
                            })
                            .then(response => {
                                if (!response.ok) {
                                    throw new Error(response.statusText)

                                }
                                return response.json()
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        console.log(result)
                        Swal.fire({
                            icon: `${result.value.icon}`,
                            title: `${result.value.msg}`,
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then((result) => {
                            // Reload the Page
                            location.reload();
                        });
                    }
                })
            });

            $("#add-hermesid").click(function(e) {
                e.preventDefault();
                const orden = document.querySelector('#add-hermesid');
                var token =
                    '{{ csrf_token() }}';

                Swal.fire({
                    title: 'Añadir HERMES ID',
                    text: "Agregue el HERMES ID a la orden.",
                    icon: 'info',
                    input: 'text',
                    showCancelButton: true,
                    confirmButtonText: 'Aceptar',
                    showLoaderOnConfirm: true,
                    inputValidator: function(option) {
                        return new Promise(function(resolve, reject) {
                            if (option != '' || option != null) {
                                resolve();
                            } else {
                                reject('El campo no debe quedar vacio');
                            }
                        });
                    },
                    preConfirm: (option) => {
                        let data = {
                            id: orden.dataset.id,
                            opcion: option,
                            _token: token
                        };
                        return fetch('{{ route('order.hermesid') }}', {
                                headers: {
                                    'Content-Type': 'application/json',
                                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                                },
                                method: 'POST',
                                body: JSON.stringify(data)
                            })
                            .then(response => {
                                if (!response.ok) {
                                    throw new Error(response.statusText)

                                }
                                return response.json()
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {
                        console.log(result)
                        // Swal.fire({
                        //     icon: `${result.value.icon}`,
                        //     title: `${result.value.msg}`,
                        //     allowOutsideClick: false,
                        //     allowEscapeKey: false
                        // }).then((result) => {
                        //     // Reload the Page
                        //     location.reload();
                        // });
                    }
                })
            });
            $("#add-rfc").click(function(e) {
                e.preventDefault();
                const orden = document.querySelector('#add-rfc');
                Swal.fire({
                    title: 'Actualizar RFC a cliente',
                    text: "Agregue el RFC a la orden.",
                    // icon: 'info',
                    // input: 'text',
                    html:
                    `<label for="swal2-input" class="swal2-input-label">RFC</label>
                    <input id="swal-input1" class="swal2-input">`+
                    `<label for="swal2-input" class="swal2-input-label">Num. Regimen Fiscal</label><select id="swal-input2" class="swal2-select">
                        <option value=''>Seleccionar</option>
                        <option value="601">601 - General de Ley Personas Morales</option>
                        <option value="603">603 - Personas Morales con Fines no Lucrativos</option>
                        <option value="605">605 - Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                        <option value="606">606 - Arrendamiento</option>
                        <option value="607">607 - Régimen de Enajenación o Adquisición de Bienes</option>
                        <option value="608">608 - Demás ingresos</option>
                        <option value="610">610 - Residentes en el Extranjero sin Estamblecimento Permanente en México</option>
                        <option value="611">611 - Ingresos por Dividendos (socios y accionistas)</option>
                        <option value="612">612 - Personas Físicas con Actividades Empresariales y Profesionales</option>
                        <option value="614">614 - Ingresos por intereses</option>
                        <option value="615">615 - Régimen de los ingresos por obtención de premios</option>
                        <option value="616">616 - Sin obligaciones fiscales</option>
                        <option value="620">620 - Sociedades Cooperativas de Producción que optan por difereir sus ingresos</option>
                        <option value="621">621 - Incorporación Fiscal</option>
                        <option value="622">622 - Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                        <option value="623">623 - Opcional para Grupos de Sociades</option>
                        <option value="624">624 - Coodinados</option>
                        <option value="625">625 - Régimen de las Actividades Empresariales con ingresos a travéz de Plataformas Tecnológicas</option>
                        <option value="626">626 - Régimen Simplicado de Confianza</option>
                      </select>`,
                    showCancelButton: true,
                    confirmButtonText: 'Aceptar',
                    showLoaderOnConfirm: true,
                    // inputValidator: function(option) {
                    //     return new Promise(function(resolve, reject) {
                    //         if (option != '' || option != null) {
                    //             resolve();
                    //         } else {
                    //             reject('El campo no debe quedar vacio');
                    //         }
                    //     });
                    // },
                    focusConfirm: false,
                    preConfirm: () => {
                        return new Promise(function (resolve, reject) {
                            // Validate input
                            if ($('#swal-input1').val() == '' || $('#swal-input2').val() == '') {
                                Swal.disableButtons();	 // Enable the confirm button again.
                                Swal.hideLoading()	
                                swal.showValidationMessage("Ingrese valores validos"); // Show error when validation fails.
                            } else {
                                swal.resetValidationMessage(); // Reset the validation message.
                                resolve(sendRFC(orden.dataset.id,$('#swal-input1').val(),$('#swal-input2').val()));
                            }
                        })
                        // console.log(resolve)
                    },
                    // allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    // console.log(JSON.stringify(result.value[0]));
                    // console.log(JSON.stringify(result.value[0]));
                    if (result.isConfirmed) {
                        console.log(result)
                        Swal.fire({
                            icon: `${result.value.icon}`,
                            title: `${result.value.msg}`,
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then((result) => {
                            // Reload the Page
                            location.reload();
                        });
                    }
                })
            });

            function sendRFC(order, rfc, code)
            {
                var token =
                    '{{ csrf_token() }}';
                let data = {
                            id: order,
                            rfc: rfc,
                            code: code,
                            _token: token
                        };
                        return fetch('{{ route('order.add-rfc') }}', {
                                headers: {
                                    'Content-Type': 'application/json',
                                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                                },
                                method: 'POST',
                                body: JSON.stringify(data)
                            })
                            .then(response => {
                                if (!response.ok) {
                                    throw new Error(response.statusText)

                                }
                                return response.json()
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            })
            }
            
        });
    </script>
@endpush
