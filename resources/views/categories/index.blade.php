@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-6  offset-md-3">
        <!-- Page Header -->
        <div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
            <h1 class="page-title fw-semibold fs-18 mb-0">Productos Google/Facebook</h1>
            <div class="ms-md-1 ms-0">
                <nav>
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Productos</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
  <!-- Start::row-1 -->
  <div class="row">
    <div class="col-xl-6  offset-md-3">
        <div class="card custom-card">
                <div class="card-header justify-content-between">
                    <div class="card-title">
                        Categorias
                    </div>
                    <a href="{{ route('category.create') }}" class="btn btn-primary">Nueva Categoria</a>
                </div>
            <div class="card-body">
                <table class="table table-responsive mb-0" id="categories-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Categorias</th>
                            <th>ID Google</th>
                            <th>opcion</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!--End::row-1 -->

@endsection
@include('assets.datatables')
@push('scripts')
<script>
    $(function() {
        $('#categories-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('category.data') !!}',
            columns: [
                {
                    data: 'id',
                    name: 'id'
                },
                { 
                    data: 'categories',
                    name: 'categories'
                },
                { 
                    data: 'id_google',
                    name: 'id_google'
                },
                {
                    data: 'options',
                    name: '',
                    orderable: false,
                    searchable: false
                }

            ],
            order: [
                [0, 'asc']
            ]
        });

      
    });
</script>
@endpush