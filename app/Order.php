<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    // protected $table = 'orders';
    protected $primaryKey = 'id_orden';
    public $incrementing = false;
    public $timestamps = false;
    protected $filable = [
        'tipo_orden', 'orden_num', 'fecha_pedido', 'nombre', 'apellido_1', 'apellido_2', 'email_envio',
        'importe_total', 'payment_method', 'payment_status', 'id_orden_hermes', 'requiere_factura', 'cliente_rfc',
        'shipping_method', 'shipping_price', 'cac_store_id', 'order_status', 'hermes_herror', 'total_discounts', 'checkout_id',
        'json_msg_creation', 'json_msg_cancelation', 'order_maskID', 'folio', 'cfdi_code',
    ];

    public function scopePendingOrders($query)
    {
        //
    }

    public function getFullNameCustomerAttribute()
    {
        return "{$this->nombre} {$this->apellido_1} {$this->apellido_2}";
    }

}
