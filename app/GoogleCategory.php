<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoogleCategory extends Model
{
    protected $table = 'google_categories';
    protected $guarded = [];
}
