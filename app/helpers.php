<?php

use App\EdicomBill;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Mtownsend\XmlToArray\XmlToArray;

if (!function_exists('unidad')) {
    function unidad($numuero)
    {
        switch ($numuero) {
            case 9:
                {
                    $numu = "NUEVE";
                    break;
                }
            case 8:
                {
                    $numu = "OCHO";
                    break;
                }
            case 7:
                {
                    $numu = "SIETE";
                    break;
                }
            case 6:
                {
                    $numu = "SEIS";
                    break;
                }
            case 5:
                {
                    $numu = "CINCO";
                    break;
                }
            case 4:
                {
                    $numu = "CUATRO";
                    break;
                }
            case 3:
                {
                    $numu = "TRES";
                    break;
                }
            case 2:
                {
                    $numu = "DOS";
                    break;
                }
            case 1:
                {
                    $numu = "UNO";
                    break;
                }
            case 0:
                {
                    $numu = "";
                    break;
                }
        }
        return $numu;
    }
}
if (!function_exists('decena')) {
    function decena($numdero)
    {

        if ($numdero >= 90 && $numdero <= 99) {
            $numd = "NOVENTA ";
            if ($numdero > 90) {
                $numd = $numd . "Y " . (unidad($numdero - 90));
            }

        } else if ($numdero >= 80 && $numdero <= 89) {
            $numd = "OCHENTA ";
            if ($numdero > 80) {
                $numd = $numd . "Y " . (unidad($numdero - 80));
            }

        } else if ($numdero >= 70 && $numdero <= 79) {
            $numd = "SETENTA ";
            if ($numdero > 70) {
                $numd = $numd . "Y " . (unidad($numdero - 70));
            }

        } else if ($numdero >= 60 && $numdero <= 69) {
            $numd = "SESENTA ";
            if ($numdero > 60) {
                $numd = $numd . "Y " . (unidad($numdero - 60));
            }

        } else if ($numdero >= 50 && $numdero <= 59) {
            $numd = "CINCUENTA ";
            if ($numdero > 50) {
                $numd = $numd . "Y " . (unidad($numdero - 50));
            }

        } else if ($numdero >= 40 && $numdero <= 49) {
            $numd = "CUARENTA ";
            if ($numdero > 40) {
                $numd = $numd . "Y " . (unidad($numdero - 40));
            }

        } else if ($numdero >= 30 && $numdero <= 39) {
            $numd = "TREINTA ";
            if ($numdero > 30) {
                $numd = $numd . "Y " . (unidad($numdero - 30));
            }

        } else if ($numdero >= 20 && $numdero <= 29) {
            if ($numdero == 20) {
                $numd = "VEINTE ";
            } else {
                $numd = "VEINTI" . (unidad($numdero - 20));
            }

        } else if ($numdero >= 10 && $numdero <= 19) {
            switch ($numdero) {
                case 10:
                    {
                        $numd = "DIEZ ";
                        break;
                    }
                case 11:
                    {
                        $numd = "ONCE ";
                        break;
                    }
                case 12:
                    {
                        $numd = "DOCE ";
                        break;
                    }
                case 13:
                    {
                        $numd = "TRECE ";
                        break;
                    }
                case 14:
                    {
                        $numd = "CATORCE ";
                        break;
                    }
                case 15:
                    {
                        $numd = "QUINCE ";
                        break;
                    }
                case 16:
                    {
                        $numd = "DIECISEIS ";
                        break;
                    }
                case 17:
                    {
                        $numd = "DIECISIETE ";
                        break;
                    }
                case 18:
                    {
                        $numd = "DIECIOCHO ";
                        break;
                    }
                case 19:
                    {
                        $numd = "DIECINUEVE ";
                        break;
                    }
            }
        } else {
            $numd = unidad($numdero);
        }

        return $numd;
    }
}

if (!function_exists('centena')) {
    function centena($numc)
    {
        if ($numc >= 100) {
            if ($numc >= 900 && $numc <= 999) {
                $numce = "NOVECIENTOS ";
                if ($numc > 900) {
                    $numce = $numce . (decena($numc - 900));
                }

            } else if ($numc >= 800 && $numc <= 899) {
                $numce = "OCHOCIENTOS ";
                if ($numc > 800) {
                    $numce = $numce . (decena($numc - 800));
                }

            } else if ($numc >= 700 && $numc <= 799) {
                $numce = "SETECIENTOS ";
                if ($numc > 700) {
                    $numce = $numce . (decena($numc - 700));
                }

            } else if ($numc >= 600 && $numc <= 699) {
                $numce = "SEISCIENTOS ";
                if ($numc > 600) {
                    $numce = $numce . (decena($numc - 600));
                }

            } else if ($numc >= 500 && $numc <= 599) {
                $numce = "QUINIENTOS ";
                if ($numc > 500) {
                    $numce = $numce . (decena($numc - 500));
                }

            } else if ($numc >= 400 && $numc <= 499) {
                $numce = "CUATROCIENTOS ";
                if ($numc > 400) {
                    $numce = $numce . (decena($numc - 400));
                }

            } else if ($numc >= 300 && $numc <= 399) {
                $numce = "TRESCIENTOS ";
                if ($numc > 300) {
                    $numce = $numce . (decena($numc - 300));
                }

            } else if ($numc >= 200 && $numc <= 299) {
                $numce = "DOSCIENTOS ";
                if ($numc > 200) {
                    $numce = $numce . (decena($numc - 200));
                }

            } else if ($numc >= 100 && $numc <= 199) {
                if ($numc == 100) {
                    $numce = "CIEN ";
                } else {
                    $numce = "CIENTO " . (decena($numc - 100));
                }

            }
        } else {
            $numce = decena($numc);
        }

        return $numce;
    }
}

if (!function_exists('miles')) {
    function miles($nummero)
    {
        if ($nummero >= 1000 && $nummero < 2000) {
            $numm = "MIL " . (centena($nummero % 1000));
        }
        if ($nummero >= 2000 && $nummero < 10000) {
            $numm = unidad(Floor($nummero / 1000)) . " MIL " . (centena($nummero % 1000));
        }
        if ($nummero < 1000) {
            $numm = centena($nummero);
        }

        return $numm;
    }
}

if (!function_exists('decmiles')) {
    function decmiles($numdmero)
    {
        if ($numdmero == 10000) {
            $numde = "DIEZ MIL";
        }

        if ($numdmero > 10000 && $numdmero < 20000) {
            $numde = decena(Floor($numdmero / 1000)) . "MIL " . (centena($numdmero % 1000));
        }
        if ($numdmero >= 20000 && $numdmero < 100000) {
            $numde = decena(Floor($numdmero / 1000)) . " MIL " . (miles($numdmero % 1000));
        }
        if ($numdmero < 10000) {
            $numde = miles($numdmero);
        }

        return $numde;
    }
}

if (!function_exists('cienmiles')) {
    function cienmiles($numcmero)
    {
        if ($numcmero == 100000) {
            $num_letracm = "CIEN MIL";
        }

        if ($numcmero >= 100000 && $numcmero < 1000000) {
            $num_letracm = centena(Floor($numcmero / 1000)) . " MIL " . (centena($numcmero % 1000));
        }
        if ($numcmero < 100000) {
            $num_letracm = decmiles($numcmero);
        }

        return $num_letracm;
    }
}

if (!function_exists('millon')) {
    function millon($nummiero)
    {
        if ($nummiero >= 1000000 && $nummiero < 2000000) {
            $num_letramm = "UN MILLON " . (cienmiles($nummiero % 1000000));
        }
        if ($nummiero >= 2000000 && $nummiero < 10000000) {
            $num_letramm = unidad(Floor($nummiero / 1000000)) . " MILLONES " . (cienmiles($nummiero % 1000000));
        }
        if ($nummiero < 1000000) {
            $num_letramm = cienmiles($nummiero);
        }

        return $num_letramm;
    }
}

if (!function_exists('decmillon')) {
    function decmillon($numerodm)
    {
        if ($numerodm == 10000000) {
            $num_letradmm = "DIEZ MILLONES";
        }

        if ($numerodm > 10000000 && $numerodm < 20000000) {
            $num_letradmm = decena(Floor($numerodm / 1000000)) . "MILLONES " . (cienmiles($numerodm % 1000000));
        }
        if ($numerodm >= 20000000 && $numerodm < 100000000) {
            $num_letradmm = decena(Floor($numerodm / 1000000)) . " MILLONES " . (millon($numerodm % 1000000));
        }
        if ($numerodm < 10000000) {
            $num_letradmm = millon($numerodm);
        }

        return $num_letradmm;
    }
}

if (!function_exists('cienmillon')) {
    function cienmillon($numcmeros)
    {
        if ($numcmeros == 100000000) {
            $num_letracms = "CIEN MILLONES";
        }

        if ($numcmeros >= 100000000 && $numcmeros < 1000000000) {
            $num_letracms = centena(Floor($numcmeros / 1000000)) . " MILLONES " . (millon($numcmeros % 1000000));
        }
        if ($numcmeros < 100000000) {
            $num_letracms = decmillon($numcmeros);
        }

        return $num_letracms;
    }
}

if (!function_exists('milmillon')) {
    function milmillon($nummierod)
    {
        if ($nummierod >= 1000000000 && $nummierod < 2000000000) {
            $num_letrammd = "MIL " . (cienmillon($nummierod % 1000000000));
        }
        if ($nummierod >= 2000000000 && $nummierod < 10000000000) {
            $num_letrammd = unidad(Floor($nummierod / 1000000000)) . " MIL " . (cienmillon($nummierod % 1000000000));
        }
        if ($nummierod < 1000000000) {
            $num_letrammd = cienmillon($nummierod);
        }

        return $num_letrammd;
    }
}

if (!function_exists('convertir')) {
    function convertir($numero)
    {
        $num = number_format($numero, 1);
        $tempnum = explode('.', $num);

        if ($tempnum[0] !== "") {
            $numf = milmillon($tempnum[0]);
            if ($numf == "UNO") {
                $numf = substr($numf, 0, -1);
                $Ps = " PESO ";
            } else {
                $Ps = " PESOS ";
            }
            $TextEnd = $numf;
            $TextEnd .= $Ps;
        }
        if ($tempnum[1] == "" || $tempnum[1] >= 100) {
            $centavos = "00";
        } else {
            $centavos = $tempnum[1];
        }
        $TextEnd .= $centavos;
        $TextEnd .= "/100 M.N.";
        return $TextEnd;
    }
}

if(!function_exists('get_bill_status'))
{
    function get_bill_status($value)
    {
        $status = '';
        $class = '';
        switch ($value) {
            case 'confirmed':
                $status = 'Confirmado';
                $class = 'success';
                break;
            case 'generated':
                $status ='Generada';
                $class = 'dark';
                case 'error':
                $status = 'Error';
                $class = 'danger';
                break;
        }

        return '<span class="badge badge-'.$class.' text-4">'.$status.'</span>';
    }
}

if(!function_exists('get_string_between'))
{
    function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) {
            return '';
        }

        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}

if(!function_exists('verify_webhook'))
{
    function verify_webhook($data, $hmac_header)
    {
        $calculated_hmac = base64_encode(hash_hmac('sha256', $data, config('app.shopify_webhook_sign'), true));
        return hash_equals($hmac_header, $calculated_hmac);
    }
}

if(!function_exists('paymentMethod'))
{
    function paymentMethod($paymentMethod, $typeOrder = '')
    {
        $return = [];
        if (
            $paymentMethod == 'Bank Deposit' ||
            $paymentMethod == 'Depósito Bancario' 
            ) {
        $return['forma_pago'] = '21';
        $return['bill_payment_method'] = '03';
        }else {
            $return['forma_pago'] = '01';
            $return['bill_payment_method'] = '04';
        }

        return $return;
    }
}

if(!function_exists('getShopifyURLForStore'))
{
    function getShopifyURLForStore($endpoint) {
        return  'https://'.config('app.shop_name').'.myshopify.com/admin/api/'.config('app.api_ver').'/'.$endpoint;
        // return  'https://test-piagui.myshopify.com/admin/api/2023-01/'.$endpoint;
    }
}


if(!function_exists('getShopifyHeadersForStore'))
{
    function getShopifyHeadersForStore() {
        return [
            'Content-Type' => 'application/json',
            'X-Shopify-Access-Token' => config('app.api_token')
        ];
        // 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'
    }
}

if(!function_exists('getShopifyHeadersForStore'))
{
    function getShopifyHeadersForStore($store, $method = 'GET') {
        return [
            'Content-Type: application/json',
            'X-Shopify-Access-Token: '. config('app.api_token')
        ];
    }
}

if(!function_exists('getCustomerData'))
{
    function getCustomerData($data){

        $customer = [];
        if(isset($data->customer) && (
            $data->customer->first_name == '' || 
            $data->customer->last_name == '' || 
            $data->customer->default_address->first_name == '' || 
            $data->customer->default_address->last_name == ''
        )){
            $customer['nombre_completo'] = $data->shipping_address->first_name;
            $customer['strApellidos'] = $data->shipping_address->last_name;
            $customer['email_envio'] = $data->email;
        }else{
            $customer['nombre_completo'] = $data->customer->first_name;
            $customer['strApellidos'] = $data->customer->last_name;
            $customer['email_envio'] = $data->customer->email;
        }

        if( strpos($customer['strApellidos'],'/') <> '' ){
            $apellidosArray = explode("/",$customer['strApellidos']);
            $customer['apellido1'] = $apellidosArray[0];
            $customer['apellido2'] = $apellidosArray[1];
        }
        else{
            $customer['apellido1'] = $customer['strApellidos'];
            $customer['apellido2'] = '';
        }


        return $customer;
    }
}

if(!function_exists('getBillingAddress'))
{
    function getBillingAddress($data){
        if (isset($data->billing_address)) {
            $dataBillingAddress = $data->billing_address;
        }else{
            $dataBillingAddress = $data->shipping_address;
        }

        return $dataBillingAddress;
    }
}

if(!function_exists('getNumbersExInt'))
{
    function getNumbersExInt($data){
        $array = [];
        $strNumeros = $data;

        if( strpos($strNumeros,'/') <> '' ){
            $numerosArray = explode("/",$strNumeros);
            $array['numExterior'] = $numerosArray[0];
            $array['numInterior'] = $numerosArray[1];
        }
        else{
            $array['numExterior'] = $strNumeros;
            $array['numInterior'] = '';
        }

        return $array;
    }
}

if(!function_exists('getAttributes'))
{
    function getAttributes($data){
        $orderAttr = $data->note_attributes;
        $orderAttrArray = [];
        $orderAttrArray2 = [];
    
        foreach($orderAttr as $attribute){
            foreach($attribute as $key=>$value){
                if($key == 'name'){
                    $keyAttrArray[] = $value;
                }
                if($key == 'value'){
                    $valueAttrArray[] = $value;
                }
            }
        }

        $orderAttributes = array_combine($keyAttrArray, $valueAttrArray);
        // $attrJson = json_encode($orderAttributes);

        return $orderAttributes;
    }
}

if(!function_exists('makeAnAPICallToShopify'))
{
    function makeAnAPICallToShopify($method, $endpoint, $url_params = null, $headers, $requestBody = null) {
        //Headers
        /**
         * Content-Type: application/json
         * X-Shopify-Access-Token: value
         */
        //Log::info('Endpoint '.$endpoint);
        try {
            $client = new Client();
            $response = null;
            if($method == 'GET' || $method == 'DELETE') {
                $response = $client->request($method, $endpoint, [ 'headers' => $headers ]);
            } else {
                $response = $client->request($method, $endpoint, [ 'headers' => $headers, 'json' => $requestBody ]);
            } 
            return [
                'statusCode' => $response->getStatusCode(),
                'body' => json_decode($response->getBody(), true),
                //'headers' => $response->getHeaders()
            ];
        } catch(\Exception $e) {
            return [
                'statusCode' => $e->getCode(),
                'message' => $e->getMessage(),
                'body' => null
            ];
        }
    }
}

if(!function_exists('tableClass')){
    function tableClass($tipo_serie)
    {
        $class = '';
        if($tipo_serie == config('app.SERIE_FACTURA')){
            $class = "table-active-success";
        }elseif($tipo_serie == config('app.SERIE_NC')){
            $class = "table-active-danger";
        }

        return $class;
    }
}