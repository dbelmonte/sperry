<?php

namespace App\Console\Commands;

use App\ProductFeed;
use Illuminate\Console\Command;

class ProductsFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:feed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Productos feed google y facebook';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $nextPageToken = null;
        do{
            $response = ShopifyRequest('get','products.json?&limit=250&page_info='.$nextPageToken);
            foreach($response['resource'] as $key => $product){
                $save_products = $this->saveProduct($product);
            }
            $nextPageToken = $response['next']['page_token'] ?? null;
        }while($nextPageToken != null);

        return 'productos importados correctamente';
    }

    public function saveProduct($data)
    {
        $product = $this->get_product_info($data);
        $chatbot = ProductFeed::updateOrCreate(
            [
                'upc_barcode' => $product['upc_barcode']
            ],
            $product
        );
    }

    public function get_product_info($product)
    {
        $active = 0;
        $price = 0;
        $compare_price = 0;
        $fecha_promo = '';
        $variantes = $product['variants'];
        $variant_title = $variantes[0]['title'];
        $sku = getCodigoSap($variantes[0]['sku']);

        if ($variantes[0]['compare_at_price'] > $variantes[0]['price']) {
            $price = $variantes[0]['compare_at_price'] . 'MXN';
            $compare_price = $variantes[0]['price'] . 'MXN';
            $fecha_promo = $product['updated_at'] . '/9999-12-31T23:59:00';
         }else{
             $price = $variantes[0]['price'] . 'MXN';
         }

        $nombre = getModelProduct($product['tags']);
        $descripcion = strip_tags($product['body_html']);
        $url_product = productUrl($product['handle']);
        $product_img = imgUrl($product);
        $marca = $product['vendor'];
        $material = getVariants($variant_title, 2);
        $disponibilidad = 'EN EXISTENCIA';
        $stock = calculateStock($variantes);
        $genero = getGender($product);
        $edad = getAge($product);
        $categoria = $product['product_type'];
        $categoria_2 = getCategoryIdGoogle($product['product_type']);
        $installment_amount = aplazo_amount($product, $variantes);
        $talla = sizeStock($variantes);
        $color = getVariants($variant_title, 0);
        
        if ($product['status'] == 'draft') {
            $active = 1;
        }

     
        return [
            'nombre' => $nombre,
            'descripcion' => $descripcion,
            'url_product' => $url_product,
            'url_imagen'=> $product_img,
            'precio' => $price,
            'precio_oferta' => $compare_price,
            'fecha_promo' => $fecha_promo,
            'marca' => $marca,
            'material' => $material,
            'disponibilidad' => $disponibilidad,
            'stock' => $stock,
            'genero' => $genero,
            'edad' => $edad,
            'categoria' => $categoria,
            'categoria_2' => $categoria_2,
            'installment_amount' => $installment_amount,
            'talla' => $talla,
            'sistema_talla' => 'MX',
            'upc_barcode' => $sku,
            'color' => $color,
            'active' => $active
        ];


    }
}
