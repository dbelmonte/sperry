<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EdicomBill extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $table = "edicom_bills";
    protected $fillable = [
        'folio', 'serie', 'id_orden', 'orden_num', 'bill_status', 'rfc', 'email', 'file_contents', 'file_b64', 'verification_code',
        'bill_type', 'error_msg', 'created', 'updated', 'order_maskID',
    ];
    protected $primaryKey = 'folio';
    public $timestamps = false;
    protected $guarded = array('folio', 'serie');

}
