<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QryProductBillCalc extends Model
{
    protected $table = 'qry_product_bill_calc';
    public $timestamps = false;
    protected $primaryKey = 'id_orden';
    public $incrementing = false;
    protected $fillable = [
        'line_item_id', 'id_orden', 'product_status', 'importe', 'total_discount', 'cantidad', 'linea_precio_unitario', 'linea_importe', 'linea_monto_descuento',
        'linea_precio_unitario_sin_descuento', 'linea_monto_iva', 'linea_importe_con_impuesto', 'linea_misc37',
    ];
}
