<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderRefund extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $table = 'order_refunds';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $guarded = [];
}
