<?php

namespace App\Http\Controllers;

class EdicomBillLine extends Controller
{
    public $Linea_Descripcion = "P102308 PINK LEATHER";
    public $Linea_Cantidad = 1;
    public $Linea_Unidad = "PAA";
    public $Linea_PrecioUnitario = 550.86;
    public $Linea_Importe = 550.86;
    public $Linea_Aduana_NumDoc;
    public $Linea_Aduana_FechaDoc;
    public $Linea_Aduana_Nombre;
    public $Linea_CuentaPredial_Numero;
    public $Linea_FraccionArancelaria;

    public $Linea_Nota;
    public $Linea_Cod_UPC;
    public $Linea_Piezas_Empaque;
    public $Linea_Cod_DUN;
    public $Linea_Cod_Barras = "117208100129";
    public $Linea_Cod_Articulo;
    public $Linea_Cod_Desc;
    public $Linea_Porc_Desc;
    public $Linea_Monto_Desc = 550.86;
    public $Linea_PrecioUnitario_SinDesc = 550.86;
    public $Linea_Cant_Empaques_Fac = 1;

    public $Linea_Cant_Empaques_Emb = 1;
    public $Linea_Porc_IVA = "0.160000";
    public $Linea_Monto_IVA = 0;
    public $Linea_Porc_IEPS;
    public $Linea_Monto_IEPS;
    public $Linea_PrecioUnitario_ConImp;
    public $Linea_Importe_ConImp = 0;
    public $Linea_Frontera;
    public $Línea_PaisOrigen;
    public $Linea_EAN_Aduanal;

    public $Linea_Misc01;
    public $Linea_Misc02;
    public $Linea_Misc03;
    public $Linea_Misc04 = "SNEAKER KIDS BINKS CAT P102306";
    public $Linea_Misc05 = 21.0;
    public $Linea_Misc06 = "ROSA";
    public $Linea_Misc07;
    public $Linea_Misc08;
    public $Linea_Misc09;
    public $Linea_Misc10;

    public $Linea_Misc11;
    public $Linea_Misc12;
    public $Linea_Misc13;
    public $Linea_Misc14;
    public $Linea_Misc15;
    public $Linea_Misc16;
    public $Linea_Misc17;
    public $Linea_Misc18;
    public $Linea_Misc19;
    public $Linea_Misc20;

    public $Linea_Misc21;
    public $Linea_Misc22;
    public $Linea_Misc23;
    public $Linea_Misc24;
    public $Linea_Misc25;
    public $Linea_Misc26;
    public $Linea_Misc27;
    public $Linea_Misc28 = "01010101";
    public $Linea_Misc29 = "PR";
    public $Linea_Misc30 = "002";

    public $Linea_Misc31 = "Tasa";
    public $Linea_Misc32;
    public $Linea_Misc33;
    public $Linea_Misc34;
    public $Linea_Misc35;
    public $Linea_Misc36;
    public $Linea_Misc37 = 0;
    public $Linea_Misc38;
    public $Linea_Misc39;
    public $Linea_Misc40;

    public $Linea_Misc41;
    public $Linea_Misc42;
    public $Linea_Misc43;
    public $Linea_Misc44;
    public $Linea_Misc45;
    public $Linea_Misc46;
    public $Linea_Misc47;
    public $Linea_Misc48;
    public $Linea_Misc49;
    public $Linea_Misc50;

    public $Linea_MedicionSecundaria;
    public $Linea_TipoIdent_Adicional;
    public $Linea_DescripIdioma;
    public $Linea_Cant_Adicional;
    public $Linea_Cant_Adicional_Tipo;
    public $Linea_Tipo_Referencia;
    public $Linea_Calif_NumIdentidad;
    public $Linea_TipoEmpaquetado;
    public $Linea_Metodo_Pago;
    public $Linea_Numero_Lote;

    public $Linea_Fecha_ProdLote;
    public $Linea_Ind_CargoDescuento;
    public $Linea_Inf_CargoDescuento;
    public $Línea_Secuencia_Calculo;
    public $Linea_Tipo_ServiciosEsp;
    public $Linea_Ident_Impuesto;
    public $Linea_Cod_EAN = "801100499097";
    public $Linea_NoIdentificacion = "801100499097";

    public $ObjImp = "02";

    public function buildConcept(): string
    {

        $formato = '¬%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
        '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
        '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
        '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
        '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
        '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
        '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
        '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
        '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|' .
        '%s|%s|%s|%s|%s|%s|%s|%s|%s|%s' . chr(13) . chr(10);
        //'%s|%s|%s|%s|%s|%s|%s|%s|%s'.PHP_EOL.PHP_EOL;

        $dataEdicomLine3 = sprintf($formato,
            $this->Linea_Descripcion,
            $this->Linea_Cantidad,
            $this->Linea_Unidad,
            $this->Linea_PrecioUnitario,
            $this->Linea_Importe,
            $this->Linea_Aduana_NumDoc,
            $this->Linea_Aduana_FechaDoc,
            $this->Linea_Aduana_Nombre,
            $this->Linea_CuentaPredial_Numero,
            $this->Linea_FraccionArancelaria,
            $this->Linea_Nota,
            $this->Linea_Cod_UPC,
            $this->Linea_Piezas_Empaque,
            $this->Linea_Cod_DUN,
            $this->Linea_Cod_Barras,
            $this->Linea_Cod_Articulo,
            $this->Linea_Cod_Desc,
            $this->Linea_Porc_Desc,
            $this->Linea_Monto_Desc,
            $this->Linea_PrecioUnitario_SinDesc,

            $this->Linea_Cant_Empaques_Fac,
            $this->Linea_Cant_Empaques_Emb,
            $this->Linea_Porc_IVA,
            $this->Linea_Monto_IVA,
            $this->Linea_Porc_IEPS,
            $this->Linea_Monto_IEPS,
            $this->Linea_PrecioUnitario_ConImp,
            $this->Linea_Importe_ConImp,
            $this->Linea_Frontera,
            $this->Línea_PaisOrigen,

            $this->Linea_EAN_Aduanal,
            $this->Linea_Misc01,
            $this->Linea_Misc02,
            $this->Linea_Misc03,
            $this->Linea_Misc04,
            $this->Linea_Misc05,
            $this->Linea_Misc06,
            $this->Linea_Misc07,
            $this->Linea_Misc08,
            $this->Linea_Misc09,

            $this->Linea_Misc10,
            $this->Linea_Misc11,
            $this->Linea_Misc12,
            $this->Linea_Misc13,
            $this->Linea_Misc14,
            $this->Linea_Misc15,
            $this->Linea_Misc16,
            $this->Linea_Misc17,
            $this->Linea_Misc18,
            $this->Linea_Misc19,

            $this->Linea_Misc20,
            $this->Linea_Misc21,
            $this->Linea_Misc22,
            $this->Linea_Misc23,
            $this->Linea_Misc24,
            $this->Linea_Misc25,
            $this->Linea_Misc26,
            $this->Linea_Misc27,
            $this->Linea_Misc28,
            $this->Linea_Misc29,

            $this->Linea_Misc30,
            $this->Linea_Misc31,
            $this->Linea_Misc32,
            $this->Linea_Misc33,
            $this->Linea_Misc34,
            $this->Linea_Misc35,
            $this->Linea_Misc36,
            $this->Linea_Misc37,
            $this->Linea_Misc38,
            $this->Linea_Misc39,

            $this->Linea_Misc40,
            $this->Linea_Misc41,
            $this->Linea_Misc42,
            $this->Linea_Misc43,
            $this->Linea_Misc44,
            $this->Linea_Misc45,
            $this->Linea_Misc46,
            $this->Linea_Misc47,
            $this->Linea_Misc48,
            $this->Linea_Misc49,

            $this->Linea_Misc50,
            $this->Linea_MedicionSecundaria,
            $this->Linea_TipoIdent_Adicional,
            $this->Linea_DescripIdioma,
            $this->Linea_Cant_Adicional,
            $this->Linea_Cant_Adicional_Tipo,
            $this->Linea_Tipo_Referencia,
            $this->Linea_Calif_NumIdentidad,
            $this->Linea_TipoEmpaquetado,
            $this->Linea_Metodo_Pago,

            $this->Linea_Numero_Lote,
            $this->Linea_Fecha_ProdLote,
            $this->Linea_Ind_CargoDescuento,
            $this->Linea_Inf_CargoDescuento,
            $this->Línea_Secuencia_Calculo,
            $this->Linea_Tipo_ServiciosEsp,
            $this->Linea_Ident_Impuesto,
            $this->Linea_Cod_EAN,
            $this->Linea_NoIdentificacion,
            $this->ObjImp
        );

        return $dataEdicomLine3;
    }
}
