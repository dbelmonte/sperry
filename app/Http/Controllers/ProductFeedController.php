<?php

namespace App\Http\Controllers;

use App\ProductFeed;
use Illuminate\Http\Request;
use App\Exports\ProductFeedExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;

class ProductFeedController extends Controller
{
    public function index()
    {
        return view('products.product_feed');
    }

    public function getDataProducts()
    {
        $producsts = ProductFeed::query()->where('stock', '>', 0)->where('active', 0);
        
        // $orders = Order::pendingOrders()->get();
        return Datatables::of($producsts)
            // ->addColumn('full_name', function ($order) {
            //     return $order->nombre . ' ' . $order->apellido_2;
            // })
            // ->addColumn('options', function ($order) {
            //     $action = '<a href="' . route('order.show', $order->id_orden) . '">ver</a>';
            //     return $action;
            // })
            // ->rawColumns(['options'])
            ->make(true);
    }

    public function export_csv() 
    {
        return Excel::download(new ProductFeedExport, 'product_feeds.csv');
    }
    public function export_excel() 
    {
        return Excel::download(new ProductFeedExport, 'product_feeds.xlsx');
    }
}
