<?php

namespace App\Http\Controllers;

use App\GoogleCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class GoogleCategoryController extends Controller
{
    public function index()
    {
       $categories = GoogleCategory::all();

       return  view('categories.index', compact('categories'));
    }

    public function getCategories()
    {
        $categories = GoogleCategory::query();

        return DataTables::of($categories)
        ->addColumn('options', function ($category) {
            $action = '<a href="' . route('category.edit', $category->id) . '" class="btn btn-icon btn-sm btn-light" taget="_blank"><i class="ri-eye-fill"></i></a>';
            return $action;
        })
        ->rawColumns(['options'])
        ->make(true);
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $category = new GoogleCategory($request->except('categories'));
        $category->categories = str_replace(', ', ',', $request->categories);
        $category->save();

        return back()->with('success', 'Categoria creada correctamente.');
    }

    public function edit(Request $request, $id)
    {
        $category = GoogleCategory::findOrFail($id);

        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        // $input = preg_split("/[\r\n,]+/", $request->categories, -1, PREG_SPLIT_NO_EMPTY);
        // $categories = implode(',', $input);
        $category = GoogleCategory::findOrFail($id);
        $category->categories = str_replace(', ', ',', $request->categories);
        $category->id_google = $request->id_google;
        $category->update();

        return back()->with('success', 'Categorias actualizadas.');
    }
}
