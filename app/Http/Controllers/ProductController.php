<?php

namespace App\Http\Controllers;

use App\Product;
use App\OrderProductNc;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function delete(Request $request)
    {
        try {
            $id_product = $request->input('id');
            $qty = $request->input('cantidad');
            $product_nc = $this->updateItemProductNc($id_product, $qty);
            $product = $this->updateItemProduct($id_product, $qty);
            if($product){
                return response()->json(['ok' => false, 'icon' => 'warning', 'msg' => 'La cantidad ingresada no puede ser mayor a la cantidad del articulo a devolver.']);
            }
            
            // return response()->json(['ok' => true, 'icon' => 'success', 'msg' => $nc_qty], 200);
            return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'Estatus de articulo cambiado correctamente'], 200);
        } catch (\Throwable $th) {
            return response()->json(['ok' => false, 'icon' => 'error', 'msg' => 'Error al intentar cambiar el estatus del articulo'], 200);
        }

    }

    public function refundShopify($id_orden, $line_item_id, $qty)
    {
        $jsonStr = '{"refund":{"shipping":{"full_refund":true},"refund_line_items":[{"line_item_id":' . $line_item_id . ',"quantity":' . $qty . ',"restock_type":"no_restock"}]}}';

        $url = "https://" . config('app.api_key') . ":" . config('app.api_pass') . "@" . config('app.shop_name') . ".myshopify.com/admin/api/" . config('app.api_ver') . "/orders/" . $id_orden . "/refunds.json";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonStr);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        $json_response = curl_exec($curl);
        curl_close($curl);

        $array_response = json_decode($json_response, true);

        if ($array_response['errors']) {
            return true;
        } else {
            return false;
        }
    }

    public function updateItemProduct($item_id, $qty)
    {
            $product = Product::where('line_item_id', '=', $item_id)->first();
            $nc_qty = $qty + $product->nota_credito;

            if ($qty == 0 || $nc_qty > $product->cantidad) {
                return true;
            }
            $product->nota_credito = $nc_qty;
            $product->product_status = 'nota_credito';
            $product->save();

            return false;

    }

    public function updateItemProductNc($item_id, $qty)
    {
        $product = OrderProductNc::where('line_item_id', '=', $item_id)->first();
        $product->cantidad = $qty;
        $product->update();

        return true;
    }
}
