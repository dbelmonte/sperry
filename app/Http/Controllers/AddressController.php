<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\OrderAddress;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function edit($id)
    {
        $address = OrderAddress::where('id_orden', $id)->where('tipo', 'billing')->first();

        return view('address.edit', compact('address'));
    }


    public function update(Request $request, $id)
    {
        try {
            $address = OrderAddress::findOrFail($id);
            $address->update($request->except(['pais']));

            return redirect()->route('order.show', $address->id_orden)->with('success', 'Dirección de cliente actualizada correctamente');
        } catch (\Throwable $th) {
           return back()->with('error', 'No se pudo actualizar la información');
        }
    }
}
