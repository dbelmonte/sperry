<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;

class CancelationController extends Controller
{
    public function Cancelar()
    {
        Artisan::call('cancelation:cron');
        return 'OK';
    }
}
