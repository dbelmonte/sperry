<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;

class ChatBotController extends Controller
{
    public function getProductsFeed()
    {
        Artisan::call('products:feed');
        return 'OK';

    }
}
