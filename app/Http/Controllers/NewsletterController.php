<?php

namespace App\Http\Controllers;

use App\Newsletter;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class NewsletterController extends Controller
{
    public function index()
    {
        return view('newsletter.index');
    }

    public function getRegisters(Request $request)
    {

        $registers = Newsletter::query();
        // Log::debug($fecha['date_before']);
        if ($request->has('params') && $request->params != 'all') {
            $fecha = json_decode($request->params, true);
            $start_date = date('Y-m-d', strtotime($fecha['date_before']));
            $end_date = date('Y-m-d', strtotime($fecha['date_after']));
            $registers->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
        }
        // $registros = $registers->select('*');
        return DataTables::of($registers)
            ->make(true);
    }

}
