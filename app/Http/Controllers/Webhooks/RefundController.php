<?php

namespace App\Http\Controllers\Webhooks;

use App\Order;
use App\Product;
use App\EdicomBill;
use App\OrderRefund;
use App\OrderProductNc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Controllers\EdicomClass;

class RefundController extends Controller
{
    public function index()
    {
        try {
            // Log::debug(json_encode($request->all()));
            $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
            $dataOrigin = file_get_contents('php://input');
            $verified = verify_webhook($dataOrigin, $hmac_header, config('app.webhook_sign'));
            if(var_export($verified, false)){
                return response()->json(['code'=> 500, 'message'=>'Verificacion de Webhook Shopify Incorrecta'], 200);
            }
    
            $data = json_decode($dataOrigin);
    
            $idRefund = $data->id; // RAZ ID DE SHOPIFY
            $idOrdenShopify = $data->order_id;
            $uuid = getUuid($idOrdenShopify);
            $refundCreatedAt = $data->created_at;
            $refundNote = $data->note;
            $refundJsonMsg = $dataOrigin;

            $order_adjustments = $data->order_adjustments;
            $refundLineItems = $data->refund_line_items;
            $isNotForNC = $this->productIsNotForNc($data->refund_line_items);

            $order = Order::where('id_orden', '=', $idOrdenShopify)->first();
            
            Log::debug('----orden----');
            Log::debug(json_encode($order));
            // $orderAdress = OrderAddress::where('id_orden', $idOrdenShopify)->where('tipo', '=', 'shipping')->first();

            if ($data->refund_line_items == null || $data->refund_line_items == '') {
                Log::info('El producto no esta preparado por lo que no se puede genera la NC');
                return response()->json(['code'=> 200, 'message'=>"El producto no esta preparado por lo que no se puede genera la NC"], 200);
            }
            if($isNotForNC){
                Log::info('El producto no esta preparado por lo que no se puede genera la NC');
                return response()->json(['code'=> 200, 'message'=>"El producto no esta preparado por lo que no se puede genera la NC"], 200);
            }
            if($order->tipo_orden == 'X'){
                Log::info('La orden ya se ha cancelado previamente.');
                return response()->json(['code'=> 200, 'message'=>"La orden ya se ha cancelado previamente."], 200);
            }

            // $validate = OrderRefund::where('id_refund', '=', $idRefund)->get();
            if(OrderRefund::where('id_refund', '=', $idRefund)->exists()){
                return response()->json(['code'=> 200, 'message'=>"La devolucion ya existe"], 200);
            }
    
            $saveRefund = OrderRefund::updateOrCreate(
            [
                'id_refund'=>$idRefund, 
            ],
            [
                'id_refund'=>$idRefund, 
                'id_orden'=>$idOrdenShopify, 
                'created_at'=> $refundCreatedAt,
                'note'=>$refundNote,
                'json_msg'=> $refundJsonMsg
            ]);

            $dateNow = date('Y-m-d H:i:s'); 

            $edicom = EdicomBill::create(
                [
                    'serie' => config('app.SERIE_NC'),
                    'id_orden' => $order->id_orden,
                    'orden_num' => $order->orden_num,
                    'bill_status' => 'generated',
                    'rfc' => $order->cliente_rfc,
                    'email' => $order->email_envio,
                    'created' => $dateNow
                ]
            );
            // $folio = $edicom->folio;

            if (empty($order_adjustments)) {
                $ShippingRefund = false;
            } else {
                $ShippingRefund = true;
                $order->flete = 1;
                $order->save();
            }

            $paymentMethod = paymentMethod($order->payment_method, $order->mc_tipo);
            $billPaymentMethod = $paymentMethod['bill_payment_method'];
            
            foreach ($refundLineItems as $lineItem) {
                $lineItemId = $lineItem->line_item_id;
                $lineItemQty = $lineItem->quantity;
                $product = Product::where('id_orden', $idOrdenShopify)->where('line_item_id', '=', $lineItemId)->first();
                $product->product_status = 'nota_credito';
                $product->nota_credito = $product->nota_credito + $lineItemQty;
                $product->save();

                $product_nc = OrderProductNc::create([
                    'folio'=> $edicom->folio,
                    'line_item_id'=>$lineItemId,
                    'id_orden'=>$idOrdenShopify,
                    'codigo_alfa'=>$product->codigo_alfa,
                    'sku'=>$product->sku,
                    'cantidad'=>$lineItemQty,
                    'importe'=>$product->importe,
                    'referencia'=>$product->codigo_alfa,
                    'talla'=>$product->talla,
                    'title'=>$product->title,
                    'total_discount'=>$product->total_discount,
                    'variant_title'=>$product->variant_title,
                ]);
                
            }

            $products = OrderProductNc::where('folio', '=', $edicom->folio)->get();
            $edicomBill = new EdicomClass();
            $edicomBill->Folio = $edicom->folio;
            $edicomBill->SerieComprobante = config('app.SERIE_NC');
            $edicomBill->idOrdenShopify = $order->id_orden;
            $edicomBill->ordenNum = $order->orden_num;
            $edicomBill->RFCReceptor = $order->cliente_rfc;
            $edicomBill->ordenEmailClient = $order->email_envio;
            $edicomBill->statusProducto = 'nota_credito';
            $edicomBill->EfectoComprobante = 'E';
            $edicomBill->TipoCFD = 'NC';
            $edicomBill->FormaPago = $billPaymentMethod;
            $edicomBill->prodctosToNC = $products;
            if ($ShippingRefund) {
                $edicomBill->envioEstandarPrecioConIva = floatval($order->shipping_price);
            } else {
                $edicomBill->envioEstandarPrecioConIva = 0;
            }
            $edicomBill->UUID = $uuid;
            $edicomBill->createBill();

            Log::info('La NC fue creada correctamente.');
            return response()->json(['code'=> 200, 'message'=> 'La NC fue creada correctamente.'], 200);


        } catch (\Exception $th) {
            Log::debug($th);
           return response()->json(['code'=> 500, 'message'=> 'Error al crear la devolucion'], 200);
        }
    }

    public function productIsNotForNc($data){
        $return = false;
        foreach($data as $item){
            if($item->line_item->fulfillment_status == null || $item->line_item->fulfillment_status == ''){
                $return =  true;
            }
        }
    
        return $return;
    }
}
