<?php

namespace App\Http\Controllers\API;

use App\Order;
use App\EdicomBill;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public function getOrders(Request $request)
    {

        $columns = array( 
            0 =>'orden_num', 
            1 =>'id_orden_hermes',
            2=> 'fecha_pedido',
            3 => 'full_name',
            4 => 'importe_total',
            5 => 'payment_status',
            6 => 'shipping_method',
            7 => 'shipping_price',
            8 => 'payment_method',
            9 => 'id_orden'
        );

        $totalData = Order::count();

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
        $orders = Order::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
        $search = $request->input('search.value'); 

        $orders =  Order::where('id_orden','LIKE',"%{$search}%")
                    ->orWhere('orden_num', 'LIKE',"%{$search}%")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        $totalFiltered = Order::where('id_orden','LIKE',"%{$search}%")
                    ->orWhere('orden_num', 'LIKE',"%{$search}%")
                    ->count();
        }

        $data = array();
        if(!empty($orders))
        {
        foreach ($orders as $order)
        {
        $show =  route('order.show',$order->id_orden);
        // $edit =  route('posts.edit',$post->id_orden);

        $nestedData['id_orden'] = $order->id_orden;
        $nestedData['orden_num'] = "<a href='https://ninewestmexico.myshopify.com/admin/orders/{$order->id_orden}' target='_blank'>{$order->orden_num}</a>";
        $nestedData['id_orden_hermes'] = $order->id_orden_hermes;
        $nestedData['fecha_pedido'] = $order->fecha_pedido;
        $nestedData['full_name'] = $order->full_name_customer;
        $nestedData['importe_total'] = $order->importe_total;
        $nestedData['payment_status'] = $order->payment_status;
        $nestedData['shipping_method'] = $order->shipping_method;
        $nestedData['shipping_price'] = $order->shipping_price;
        $nestedData['payment_method'] = $order->payment_method;

        // $nestedData['body'] = substr(strip_tags($post->body),0,50)."...";
        // $nestedData['fecha_pedido'] = date('j M Y h:i a',strtotime($order->fecha_pedido));
        // $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' >Editar</a>";
        $data[] = $nestedData;

        }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
            );

        echo json_encode($json_data); 




    }

    public function getEdicomBills(Request $request)
    {
        $columns = array( 
            0 =>'orden_num', 
            1 =>'id_orden',
            2=> 'bill_status',
            3 => 'verification_code',
            4 => 'created',
            5 => 'folio',
        );

        $totalData = EdicomBill::count();

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
        $orders = EdicomBill::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
        $search = $request->input('search.value'); 

        $orders =  EdicomBill::where('id_orden','LIKE',"%{$search}%")
                    ->orWhere('orden_num', 'LIKE',"%{$search}%")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        $totalFiltered = EdicomBill::where('id_orden','LIKE',"%{$search}%")
                    ->orWhere('orden_num', 'LIKE',"%{$search}%")
                    ->count();
        }

        $data = array();
        if(!empty($orders))
        {
        foreach ($orders as $order)
        {
        // $show =  route('order.show',$order->id_orden);
        // $edit =  route('posts.edit',$post->id_orden);

        $nestedData['id_orden'] = $order->id_orden;
        $nestedData['orden_num'] = "<a href='https://ninewestmexico.myshopify.com/admin/orders/{$order->id_orden}' target='_blank'>{$order->orden_num}</a>";
        $nestedData['status'] = $order->bill_status;
        $nestedData['verification'] = $order->verification_code;
        $nestedData['created'] = $order->created;
        $nestedData['folio'] = $order->serie .'-'.$order->folio;

        // $nestedData['body'] = substr(strip_tags($post->body),0,50)."...";
        // $nestedData['fecha_pedido'] = date('j M Y h:i a',strtotime($order->fecha_pedido));
        // $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' >Editar</a>";
        $data[] = $nestedData;

        }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
            );

        echo json_encode($json_data); 
    }
}
