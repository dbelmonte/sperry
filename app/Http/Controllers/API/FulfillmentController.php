<?php

namespace App\Http\Controllers\API;

use App\OrderTracking;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class FulfillmentController extends Controller
{
    public function getFulfillmentOrder($id)
    {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "X-Shopify-Access-Token: ".config('app.api_token')));
            curl_setopt($ch, CURLOPT_URL, 'https://'.config('app.shop_name').'.myshopify.com/admin/api/2023-01/orders/'.$id.'/fulfillment_orders.json');
            $result = curl_exec($ch);
            curl_close($ch);
            
            $response = json_decode($result, true);

            $full_order = [];
            $prods = [];
            
            foreach($response['fulfillment_orders'] as $items){
                $full_order['fulfillment_order_id'] = $items['id'];
                // $prods = array(
                //     "id"=> 'dasdasda',
                //     "line_item_id"=> 'dasdasdas'
                // );
                foreach($items['line_items'] as $item){
                    $prods[] = array(
                        'id' => $item['id'],
                        'line_item_id' => $item['line_item_id']
                    );
                }
            }
            $line_items = array(
                'line_items' => $prods
            );
            $respuesta = array_merge($full_order, $line_items);
            return json_encode($respuesta);
    }

    public function generateTracking(Request $request)
    {
       $dataItems = file_get_contents('php://input');
        Log::debug($dataItems);
        $data = json_decode($dataItems, true);
        // Log::debug(var_dump($data['line_items']));
        // $line_item_id = array(
        //     'line_item_id' => [
        //         'id'=> 12026327367829,
        //         'fulfillment_order_id'=> 5810432442517,
        //         'quantity' => 1
        //     ]
        // );

        // $request = array(
        //     'message'=> 'test message',
        //     'notify_customer' => false,
        //     'number' => '11115778148220',
        //     'url' => 'https://www.paquetexpress.com.mx/rastreo/11115778148220',
        //     'company' => 'paquetexpress'
        // );
        
        $payload = $this->getPayloadForFulfillment($data);
        $api_endpoint = 'fulfillments.json';
        
        $endpoint = getShopifyURLForStore($api_endpoint); 
        // dd(json_encode($payload));
        $headers = getShopifyHeadersForStore();

        $response = makeAnAPICallToShopify('POST', $endpoint, null, $headers, $payload); 
            Log::debug(json_encode($response));
        return response()->json($response);

    }

    public function getPayloadForFulfillment($data) {

        return [
            'fulfillment' => [
                'message' => '',
                'notify_customer' => true,
                'tracking_info' => [
                    'number' => $data['number'],
                    'url' => $data['url'],
                    'company' => $data['company']
                ],
                'line_items_by_fulfillment_order' => $this->getFulfillmentOrderArray($data['fulfillment_order_id'], $data['line_items'])
            ]
        ];
    }

    // public function getShopifyURLForStore($endpoint) {
    //     return  'https://'.config('app.shop_name').'.myshopify.com/admin/api/2023-01/'.$endpoint;
    //     // return  'https://test-piagui.myshopify.com/admin/api/2023-01/'.$endpoint;
    // }

    // public function getShopifyHeadersForStore() {
    //     // return [
    //     //     'Content-Type: application/json',
    //     //     'X-Shopify-Access-Token: '.'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'
    //     // ];
    //     // prod
    //     return [
    //         'Content-Type' => 'application/json',
    //         'X-Shopify-Access-Token' => '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'
    //     ];
    //     // test
    //     // return [
    //     //     'Content-Type' => 'application/json',
    //     //     'X-Shopify-Access-Token' => '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'
    //     // ];
    // }

    public function getFulfillmentOrderArray($idFulfillment, $line_items) {
        $temp_payload = [];
        // $search = (int) $request['lineItemId'];
        // foreach($line_items as $line_item)
        //     if($line_item['line_item_id'] === $search) 
        //         $temp_payload[] = [
        //             'fulfillment_order_id' => $line_item['fulfillment_order_id'],
        //             'fulfillment_order_line_items' => [[
        //                 'id' => $line_item['id'],
        //                 'quantity' => (int) $request['no_of_packages']
        //             ]]
        //         ];
        // $temp_payload1 = ['fulfillment_order_id' => $idFulfillment];
        foreach($line_items as $line_item){
            $temp_payload[] = [
                'fulfillment_order_id' => $idFulfillment,
                'fulfillment_order_line_items' => [[
                    'id' => $line_item['id'],
                    'quantity' => (int) $line_item['quantity']
                ]]
            ];
        }
        // $response = array_push($temp_payload1, $temp_payload);
        // Log::debug(json_encode($response));
        return $temp_payload;

        
    }

    // public function makeAnAPICallToShopify($method, $endpoint, $url_params = null, $headers, $requestBody = null) {
    //     //Headers
    //     /**
    //      * Content-Type: application/json
    //      * X-Shopify-Access-Token: value
    //      */
    //     //Log::info('Endpoint '.$endpoint);
    //     try {
    //         $client = new Client();
    //         $response = null;
    //         if($method == 'GET' || $method == 'DELETE') {
    //             $response = $client->request($method, $endpoint, [ 'headers' => $headers ]);
    //         } else {
    //             $response = $client->request($method, $endpoint, [ 'headers' => $headers, 'json' => $requestBody ]);
    //         } 
    //         return [
    //             'statusCode' => $response->getStatusCode(),
    //             'body' => json_decode($response->getBody(), true),
    //             //'headers' => $response->getHeaders()
    //         ];
    //     } catch(\Exception $e) {
    //         return [
    //             'statusCode' => $e->getCode(),
    //             'message' => $e->getMessage(),
    //             'body' => null
    //         ];
    //     }
    // }

    public function updateTrackingStatus(Request $request){
        $dataRequest = file_get_contents('php://input');
        $data = json_decode($dataRequest, true);

        $idFulfillment = $this->getFulfillmentId($data['orden_hermes']);

        // var_dump($idFulfillment);

        $payload = $this->getPayloadForUpdatedTrackingStatus($data);
        $api_endpoint = 'orders/'.$data['id_orden'].'/fulfillments/'.$idFulfillment.'/events.json';
        
        $endpoint = getShopifyURLForStore($api_endpoint); 
        $headers = getShopifyHeadersForStore();

        $response = makeAnAPICallToShopify('POST', $endpoint, null, $headers, $payload); 
        Log::debug('respuesta de tracking status');
            Log::debug(json_encode($response));
        return response()->json($response);

    }

    public function getPayloadForUpdatedTrackingStatus($data)
    {
        return [
          'event' => [
            'status' => $data['status']
          ]
        ];
    }

    public function getFulfillmentId($ordenHermes)
    {
        $idHermes = OrderTracking::where('orden_hermes', '=', $ordenHermes)->first();

        return $idHermes->id_fulfillment;
    }
    
}
