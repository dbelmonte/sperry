<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CustomerController extends Controller
{
    public function updated(Request $request)
    {
        try {
            Log::debug(json_encode($request->all()));
              $customer_id = $request->input('id');
              $first_name = $request->input('first_name');
              $last_name = $request->input('last_name');
    
              $payload = $this->getPayloadCustomer($first_name, $last_name);
              $api_endpoint = 'customers/'.$customer_id.'.json';
    
              $endpoint = getShopifyURLForStore($api_endpoint);
              $headers = getShopifyHeadersForStore();
    
              $response = makeAnAPICallToShopify('PUT', $endpoint, NULL, $headers, $payload);
            
              Log::debug(json_encode($response));
              return response()->json(['ok' => true, 'icon' => 'success', 'msg' => 'Tus datos fueron actualizados correctamente'], 200);
      
        } catch (\Throwable $th) {
            return response()->json(['ok' => false, 'icon' => 'warning', 'msg' => 'Error al actualizar los datos de cliente.'], 200);
        }
      
    }

    public function getPayloadCustomer($first_name, $last_name)
    {
        return [
            'customer'=>[
                'first_name'=> $first_name,
                'last_name'=> $last_name
            ]
        ];
    }
}
