<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = new User($request->except('password'));
            $user->setPass($request->password);
            $user->save();

            $user->assignRole($request->role);

            return back()->with('success', 'Usuario creado exitosamente.');
        } catch (\Exception $th) {
            Log::debug($th);
            return back()->with('error', 'Error al crear el usuario.');
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->setPass($request->password);
            $user->update($request->except('password'));
            
            $user->syncRoles($request->role);

            return redirect()->route('user.list')->with('success', 'Usuario actualizado correctamente');
        } catch (\Throwable $th) {
            return back()->with('error', 'No se pudo actualizar el usuario.');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $user = User::find($request->id);
            if ($user->id == 1){
                return response()->json([
                    'ok' => false, 
                    'icon' => 'warning',
                    'msg' => 'No tienes permisos para realizar esta acción.'
                ], 200);
            }
            $user->syncRoles([]);
            $user->syncPermissions([]);
            $user->delete();
            return response()->json([
                'ok' => true, 
                'icon' => 'success',
                'msg' => 'Usuario eliminado correctamente.'
            ], 200);
        }catch (\Exception $exception){
            return response()->json([
                'ok' => false, 
                'icon' => 'warning',
                'msg' => 'Intente nuevamente o comuniquese con el administrador.'
            ], 200);
        }
    }
}
