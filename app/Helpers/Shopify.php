<?php

use App\GoogleCategory;
use Illuminate\Support\Facades\Log;

if (!function_exists('ShopifyRequest')) {
    function ShopifyRequest($method,$url,$param = []){
        $client = new \GuzzleHttp\Client();
        $url = 'https://'. config('app.shop_name') .'.myshopify.com/admin/api/'. config('app.api_ver') .'/' .$url;
        $parameters = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                "X-Shopify-Access-Token" => config('app.api_token')
            ]
        ];
        if(!empty($param)){ $parameters['json'] = $param;}
        $response = $client->request($method, $url,$parameters);
        $responseHeaders = $response->getHeaders();
        // Log::debug($responseHeaders);
        $tokenType = 'next';
        if(array_key_exists('Link',$responseHeaders)){
            $link = $responseHeaders['Link'][0];
            $tokenType  = strpos($link,'rel="next') !== false ? "next" : "previous";
            $tobeReplace = ["<",">",'rel="next"',";",'rel="previous"'];
            $tobeReplaceWith = ["","","",""];
            parse_str(parse_url(str_replace($tobeReplace,$tobeReplaceWith,$link),PHP_URL_QUERY),$op);
            $pageToken = trim($op['page_info']);
        }
        if (isset($responseHeaders["X-Shopify-Shop-Api-Call-Limit"][0])) {
            $call_limit = $responseHeaders["X-Shopify-Shop-Api-Call-Limit"][0];
         }else {
             $call_limit = '1/400';
         }
        $rateLimit = explode('/', $call_limit);
        $usedLimitPercentage = (100*$rateLimit[0])/$rateLimit[1];
        if($usedLimitPercentage > 95){sleep(5);}
        $responseBody = json_decode($response->getBody(),true);
        $r['resource'] =  (is_array($responseBody) && count($responseBody) > 0) ? array_shift($responseBody) : $responseBody;
        $r[$tokenType]['page_token'] = isset($pageToken) ? $pageToken : null;
        return $r;
    }
}

if (!function_exists('getVariants')) {
    function getVariants($data, $position)
    {
        $variant = explode(' / ', $data);
        if (count($variant) < 3) {
            return 'N/A';
        }
        switch ($position) {
            case '1':
                return $variant[1];
            break;
            case '2':
                return $variant[2];
            break;
            default:
                return $variant[0];
            break;
        }
        
    }
}

if (!function_exists('getCodigoSap')) {
    function getCodigoSap($codigo)
    {
        $sap = explode("-", $codigo);

        return $sap[0];
    }
}

if (!function_exists('productUrl')) {
    function productUrl($data)
    {
        $url = config('app.shop_doamin').'products/'.$data;

        return $url;
    }
}

if (!function_exists('get_product_collections')) {
    function get_product_collections($product_id)
    {
        $api_endpoint = 'smart_collections.json?product_id='.$product_id;
        $endpoint = getShopifyURLForStore($api_endpoint); 
        $headers = getShopifyHeadersForStore();
        $response = makeAnAPICallToShopify('GET', $endpoint, null, $headers, null); 

        $collections = $response['body']['smart_collections'];
        
        $product_collections = [];
        foreach ($collections as $key => $collection) {
            $product_collections[$key] = $collection['title'];
        }

        return $product_collections;
    }
}

if (!function_exists('imgUrl')) {
    function imgUrl($data)
    {
       return  isset($data['image']['src']) ? $data['image']['src'] : 'https://placehold.co/600x400';
    }
}

if(!function_exists('calculateStock')){
    function calculateStock($data)
    {
        $total = 0;
        foreach ($data as $key => $stock) {
                $total += $stock['inventory_quantity'];
        }

        return $total;
    }
}

if (!function_exists('sizeStock')) {
    function sizeStock($variants){
        $tallas = [];
        $cm = 'cm';
        foreach ($variants as $key => $variante) {
            if ($variante['option2'] == 'Unitalla') {
               $cm = '';
            }
            if ($variante['inventory_quantity'] >= config('app.size_limit_stock')) {
                $tallas[$key] = $variante['option2'].$cm;
            }
        }

        return implode(', ', $tallas);
    }
}

if (!function_exists('getGender')) {
    function getGender($data)
    {
        $gender = '';
        $tags = explode(', ', $data['tags']);
        foreach ($tags as $key => $tag) {
            if (str_contains($tag, 'Para Mujer') || str_contains($tag, 'Para Niñas')) {
                $gender = 'female';
            }
            if (str_contains($tag, 'Para Hombre') || str_contains($tag, 'Para Niños')) {
                $gender = 'male';
            }
            if (str_contains($tag, 'Unisex')) {
                $gender = 'unisex';
            }
        }

        return $gender;
    }
}

if (!function_exists('getAge')) {
    function getAge($data){
        $age = '';
        $tags = explode(', ', $data['tags']);
        $adulto = array('Para Mujer', 'Para Hombre');
        $ninos = array('Para Niños', 'Para Niñas');
        $recien_nacidos = array('primeros_pasos');
        foreach ($tags as $key => $tag) {
            if (in_array($tag, $adulto) || str_contains($tag, 'Unisex')) {
                $age = 'adulto';
            }
            if (in_array($tag, $ninos) && !in_array($tag, $recien_nacidos)) {
                $age = 'niños';
            }
            if (in_array($tag, $recien_nacidos) && in_array($tag, $ninos)) {
                $age = 'recien_nacidos';
            }
        }

        return $age;
    }
}

if (!function_exists('getCategoryIdGoogle')) {
    function getCategoryIdGoogle($product_type){

        $google_collection = GoogleCategory::all();

        // dd($google_collection);
        $id = 0;
        foreach ($google_collection as $key => $collection) {
            if (in_array($product_type, explode(',', $collection['categories']), false)) {
                $id = $collection['id_google'];
            }
        }

        return $id;

    }
}

if (!function_exists('aplazo_amount')) {
    function aplazo_amount($product, $variant)
    {
        $aplazo = '';
        // if ($variant[0]['compare_at_price'] > $variant[0]['price']) {
            $aplazo = '6:'. round(($variant[0]['price'] / 6), 2)  . 'MXN';
        //  }else{
        //      $aplazo = '6:'. round(($variant[0]['price'] / 6), 2) . ' MXN';
        //  }

        return $aplazo;
    }
}

if (!function_exists('getModelProduct')) {
    function getModelProduct($data){
        $model = '';
        $tags = explode(', ', $data);
        foreach ($tags as $key => $tag) {
            if (str_contains($tag, 'MODELO_')) {
                $model = $tag;
            }
        }

        return str_replace("MODELO_", "", $model);
    }
}