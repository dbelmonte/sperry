<?php

use App\EdicomBill;
use Illuminate\Support\Facades\Log;
use Mtownsend\XmlToArray\XmlToArray;


if (!function_exists('getDocumentB64')) {
    function getDocumentB64($confirmation)
    {
        $xmlreq = '<?xml version="1.0" encoding="UTF-8"?>';
    /* $xmlreq .= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ediw="http://ediwinIntegration.service.ediwinws.edicom.com">';
                $xmlreq .= '<soapenv:Header/>';
                $xmlreq .= '<soapenv:Body>';
                $xmlreq .= '<ediw:getDocument>';
                $xmlreq .= '<ediw:user>'.config('app.ediwin_usr').'</ediw:user>';
                $xmlreq .= '<ediw:password>'.config('app.ediwin_passwd').'</ediw:password>';
                $xmlreq .= '<ediw:domain>'.config('app.ediwin_domain').'</ediw:domain>';
                $xmlreq .= '<ediw:exportType>2</ediw:exportType>';
                $xmlreq .= '<ediw:idVolume>0</ediw:idVolume>';
                $xmlreq .= '<ediw:parameters>';
                $xmlreq .= '<ediw:name>id</ediw:name>';
                $xmlreq .= '<ediw:value>'.$confirmation.'</ediw:value>';
                $xmlreq .= '<ediw:operator>=</ediw:operator>';
                $xmlreq .= '</ediw:parameters>';
                $xmlreq .= '</ediw:getDocument>';
                $xmlreq .= '</soapenv:Body>';
                $xmlreq .= '</soapenv:Envelope>';
        */
        $xmlreq .= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ediw="http://ediwinIntegration.service.ediwinws.edicom.com">';
        $xmlreq .= '<soapenv:Header/>';
        $xmlreq .= '<soapenv:Body>';
        $xmlreq .= '<ediw:getUnzipDocument>';
        $xmlreq .= '<ediw:user>'.config('app.ediwin_usr').'</ediw:user>';
        $xmlreq .= '<ediw:password>'.config('app.ediwin_passwd').'</ediw:password>';
        $xmlreq .= '<ediw:domain>'.config('app.ediwin_domain').'</ediw:domain>';
        $xmlreq .= '<ediw:exportType>2</ediw:exportType>';
        $xmlreq .= '<ediw:idVolume>0</ediw:idVolume>';
        $xmlreq .= '<ediw:parameters>';
        $xmlreq .= '<ediw:name>id</ediw:name>';
        $xmlreq .= '<ediw:value>'.$confirmation.'</ediw:value>';
        $xmlreq .= '<ediw:operator>=</ediw:operator>';
        $xmlreq .= '</ediw:parameters>';
        $xmlreq .= '</ediw:getUnzipDocument>';
        $xmlreq .= '</soapenv:Body>';
        $xmlreq .= '</soapenv:Envelope>';

        // Log::debug($xmlreq);
        $location_URL = "https://web.sedeb2b.com/EdiwinWS/services/EdiwinIntegration?wsdl";
        $client = new SoapClient(null, array(
            'location' => $location_URL,
            'uri' => $location_URL,
            'trace' => 1,
        ));

        try {
            $search_result = $client->__doRequest($xmlreq, $location_URL, $location_URL, 1);
    
            /* Convertir el mensaje a texto decodificando el mensaje */
            $dataXMLDecode = html_entity_decode($search_result);
            Log::debug($dataXMLDecode);
            /* -------Parse de respuesta hermes---------- */
            // $documentReturn = get_string_between($dataXMLDecode, '<getDocumentReturn>', '</getDocumentReturn>');
            $documentReturn = get_string_between($dataXMLDecode, '<getUnzipDocumentReturn>', '</getUnzipDocumentReturn>');
            $errorcode = get_string_between($dataXMLDecode, '<ns1:cod>', '</ns1:cod>');
            $errormeesaje = get_string_between($dataXMLDecode, '<ns1:text>', '</ns1:text>');
            $response_xml = $client->__getLastResponse();
            Log::debug($response_xml);
            /* TODO: corregir la respuesta de hermes  */
            if ($errorcode) {
                $response['code'] = $errorcode;
                $response['error'] = true;
                $response['message'] = $errormeesaje;
            }

            $response['code'] = 200;
            $response['documentB64'] = $documentReturn;
            
            return $response;
    
        } catch (\SoapFault $exception) {
            //var_dump(get_class($exception));
            //var_dump($exception);
            return response()->json(['code'=>400, 'error'=>$errorcode, 'message'=>$exception->getMessage()]);
    
        }
    }
}

if (!function_exists('getUuid')) {
    function getUuid($id_orden)
    {
        $bill = EdicomBill::where('id_orden', $id_orden)->where('serie', '=', config('app.SERIE_FACTURA'))->first();

        $document = getDocumentB64($bill->verification_code);

        $dataXML = base64_decode($document['documentB64']);
        $array = XmlToArray::convert($dataXML);
        return $array['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['UUID'];
    }
}
