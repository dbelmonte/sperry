<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $table = 'order_products';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $filable = [
        'line_item_id', 'id_orden', 'codigo_alfa', 'sku', 'cantidad', 'devolucion',
        'envio', 'cancelacion', 'nota_credito', 'importe', 'product_status', 'referencia',
        'talla', 'title', 'total_discount', 'variant_title', 'facturado',
    ];
}
