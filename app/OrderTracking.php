<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTracking extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $table = 'order_tracking';
    public $timestamps = false;
    protected $primaryKey = 'id_tracking';
    // protected $fillable = [
    //     'id_tracking', 'id_orden', 'tipo', 'orden_num', 'orden_hermes', 'tracking_status', 'carrier', 'titulo', 'track_number',
    // ];
    protected $guarded  = [];
}
