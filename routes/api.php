<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/automation', 'API\AutomationController@getAutomation');
Route::get('/fulfillment/order/{id}', 'API\FulfillmentController@getFulfillmentOrder');
Route::post('/fulfillment/order', 'API\FulfillmentController@generateTracking');
Route::post('/fulfillment/update/tranking/status', 'API\FulfillmentController@updateTrackingStatus');
Route::post('/customer/updated', 'API\CustomerController@updated');
Route::post('/orders', 'API\OrdersController@getOrders');
Route::post('/orders-edicom', 'API\OrdersController@getEdicomBills');

Route::prefix('webhook')->group(function () {
    Route::post('refund', 'RefundController@index');
});