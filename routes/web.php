<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('auth.login');
});

// Auth::routes();
require __DIR__ . '/auth.php';

// Route::get('/', 'HomeController@index');
Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', 'HomeController@index')->name('home');

    Route::group(['prefix' => 'order'], function () {
        Route::name('order.')->group(function () {
            // Route::get('/ordenes', 'OrderController@index');
            Route::get('order/{id_orden}', 'OrderController@show')->name('show');
            Route::get('/orders/get-orders', 'OrderController@getOrders')->name('data');
            Route::post('/order/factura', 'OrderController@facturar')->name('facturar');
            Route::post('/order/cancel', 'OrderController@cancelOrder')->name('cancel');
            Route::post('/send-hermes', 'OrderController@sendToHermes')->name('hermes');
            Route::get('/order/fechas', 'CancelationController@cancelation')->name('cancelation');
            Route::post('/order/add-hermesid', 'OrderController@addHermesId')->name('hermesid');
            Route::post('/order/add-rfc', 'OrderController@addRFC')->name('add-rfc');
            Route::get('/resend-webhook', 'OrderController@resendWebHook');
            Route::post('notacredito', 'OrderController@notacredito')->name('notacredito');
            Route::post('/creditNote', 'OrderController@creditNote')->name('gen_nc');
            Route::get('/{orderId}/nota-credito/{folio}', 'OrderController@showCreditNote')->name('show_nc');
            Route::post('/download', 'SoapController@download')->name('download');
            Route::post('/order/update-date', 'OrderController@updateDateOrder')->name('updateDate');
            
        });
        Route::group(['prefix' => 'address'], function () {
            Route::name('address.')->group(function () {
                Route::get('/address/{id}', 'AddressController@edit')->name('edit');
                Route::put('/address/{id}', 'AddressController@update')->name('update');
            });
        });
        Route::post('/product/delete', 'ProductController@delete')->name('product.delete');
    });

    Route::prefix('products')->group(function () {
        Route::name('product.')->group(function () {
            Route::get('/list-feed', 'ProductFeedController@index')->name('feed');
            Route::get('/get-feed-products', 'ProductFeedController@getDataProducts')->name('data');
            Route::get('/product-feed/export/csv', 'ProductFeedController@export_csv')->name('export_feed.csv');
            Route::get('/product-feed/export/excel', 'ProductFeedController@export_excel')->name('export_feed.excel');
        });
    });

    Route::prefix('categories')->group(function () {
        Route::name('category.')->group(function () {
            Route::get('/', 'GoogleCategoryController@index')->name('index');
            Route::get('/edit/{id}', 'GoogleCategoryController@edit')->name('edit');
            Route::get('/create', 'GoogleCategoryController@create')->name('create');
            Route::post('/store', 'GoogleCategoryController@store')->name('store');
            Route::put('/update/{id}', 'GoogleCategoryController@update')->name('update');
            Route::get('/get-categories', 'GoogleCategoryController@getCategories')->name('data');
        });
    });

    Route::group(['middleware' => ['role:Super Admin']], function () {
        Route::group(['prefix' => 'user'], function () {
            Route::name('user.')->group(function () {
                Route::get('/all', 'UserController@index')->name('list');
                Route::get('/create', 'UserController@create')->name('create');
                Route::post('/store', 'UserController@store')->name('store');
                Route::get('/edit/{id}', 'UserController@edit')->name('edit');
                Route::put('/update/{id}', 'UserController@update')->name('update');
                Route::delete('/delete', 'UserController@destroy')->name('destroy');
            });
        });
    });

    Route::get('buscar', 'OrderController@buscar')->name('search');

// Route::get('/n3YnkUWXXhUktQhP8PLH', function () {
    //     Artisan::call('canclation:cron');
    //     return 'OK';
    // });

    Route::get('/newsletter', 'NewsletterController@index')->name('newsletter');
    Route::get('/newsletter/old', 'NewsletterController@oldRegisters')->name('newsletter.old');
    Route::get('/newsletter/get-registers', 'NewsletterController@getRegisters')->name('newsletter.data');
    Route::get('/newsletter/get-registers-old', 'NewsletterController@getNewsletter')->name('newsletter.data.old');

    Route::get('qry', 'OrderController@address');

});
Route::get('/w0nXLI2WgFrCdycwvFaJ', 'CancelationController@Cancelar');
Route::get('/sin-hermesid', 'OrderController@sendHermesData');
Route::get('/ade921176492a307745bf24a8e055a3a', 'ChatBotController@getProductsFeed');